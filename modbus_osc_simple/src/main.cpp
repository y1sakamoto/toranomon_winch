#include <Arduino.h>
#include "data.h"
#include "id.h"
#include "parse.h"
#include "modbus.h"
//#include "test.h"
#include <TaskManager.h>
#include "osc.h"

void setup() {
  delay(3000);

  ID::init();
  //PARSE::setup();
  MODBUS::setup();

  OSC::init();
 /*
  Tasks.add([]() {
      MODBUS::readCurrentPos(1);
      MODBUS::readAlarm(1);
      OSC::update();
      //PARSE::update();
    })->startFps(30);
*/
    delay(3000);

}

void loop() {
  //Tasks.update();
      MODBUS::readCurrentPos(1);
      MODBUS::readAlarm(1);
      OSC::update();

}