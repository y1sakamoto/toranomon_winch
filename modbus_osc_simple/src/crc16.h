#pragma once

#include <stdio.h>
#include <vector>
using namespace std;

namespace{
namespace CRC
{

    template<typename T>
    vector<T> add(const vector<T> &data){
        vector<T> _data = data;
        T result{0};
        _data[0] = (_data[0] ^ 0xFFFF);
        for (size_t i = 0; i < _data.size(); i++)
        {
            result ^= _data[i];
            for (size_t j = 0; j < 8; j++)
            {
                if (result & 0x0001)
                {
                    result >>= 1;
                    result ^= 0xA001;
                }
                else
                    result >>= 1;
            }
        }
        T crc0 = result;
        T crc1 = result;
        crc1 = crc1 >> 8;
        crc0 = crc0 & 0xff;
        _data.clear();
        _data = data;
        _data.push_back(crc0);
        _data.push_back(crc1);
        //Serial.println(crc0,HEX);
        //Serial.println(crc1,HEX);
        return _data;
    };

/*
    int crc16(const vector<int> &_data)
    {
        vector<int> data = _data;
        int result{0};
        data[0] = (data[0] ^ 0xFFFF);
        for (int i = 0; i < data.size(); i++)
        {
            result ^= data[i];
            for (int j = 0; j < 8; j++)
            {
                if (result & 0x0001)
                {
                    result >>= 1;
                    result ^= 0xA001;
                }
                else
                    result >>= 1;
            }
        }
        unsigned int crc0 = result;
        unsigned int crc1 = result;
        crc0 = crc0 >> 8;
        crc1 = crc1 & 0xff;
        Serial.println(crc0, HEX);
        Serial.println(crc1, HEX);

        return result;
    };
*/


};
}
