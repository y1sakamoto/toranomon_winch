#pragma once
#include "data.h"
namespace ID{
 
    inline void setMAC(int8_t _mac[]){
        for (int i=0; i<6; i++)DATA::OSC::mac[i] = _mac[i]; 
        

    };

    inline void init(){
        pinMode(DATA::PIN_ASSIGN::IO_0,INPUT);
        pinMode(DATA::PIN_ASSIGN::IO_1,INPUT);
        pinMode(DATA::PIN_ASSIGN::IO_2,INPUT);
        pinMode(DATA::PIN_ASSIGN::IO_3,INPUT);
        pinMode(DATA::PIN_ASSIGN::IO_4,INPUT);
        pinMode(DATA::PIN_ASSIGN::IO_5,INPUT);
        int id{0};
        id +=  digitalRead(DATA::PIN_ASSIGN::IO_0);
        id += (digitalRead(DATA::PIN_ASSIGN::IO_1)*2);
        id += (digitalRead(DATA::PIN_ASSIGN::IO_2)*4);
        id += (digitalRead(DATA::PIN_ASSIGN::IO_3)*8);
        id += (digitalRead(DATA::PIN_ASSIGN::IO_4)*16);
        id += (digitalRead(DATA::PIN_ASSIGN::IO_5)*32);
        DATA::OSC::id = id;
        DATA::OSC::mac[5] += DATA::OSC::id;
/*
        if(id==0) setMAC(DATA::OSC::mac_0);
        if(id==1) setMAC(DATA::OSC::mac_1);
        if(id==2) setMAC(DATA::OSC::mac_2);
        if(id==3) setMAC(DATA::OSC::mac_3);
        if(id==4) setMAC(DATA::OSC::mac_4);
        if(id==5) setMAC(DATA::OSC::mac_5);
        */

    };


};