#pragma once
#include "crc16.h"
#include "config.h"





namespace{
namespace MODBUS{
    namespace ADDRESS {
        constexpr int CUR_POS = 0x00CC;
        constexpr int CUR_SPD = 0x00CE;//r/min
        constexpr int CUR_HZ  = 0x00D0;//r/min
        constexpr int CUR_ALRM= 0x0080;//r/min

    };
    namespace FUNCTION {
        constexpr int READ = 0x03;
        constexpr int WRITE_SINGLE = 0x06;
        constexpr int WRITE_MULTIPLE = 0x10;
        constexpr int READ_AND_WRITE = 0x17;

        constexpr int CHECK = 0x08;

    };
/*
    template<typename T>
    struct pubSubData{
        vector<T>d;
        void init (){d.clear();};
        void add4bite(const int data){
            unsigned int d_0 = data & 0xFF000000; d_0 = d_0 >> 24; 
            unsigned int d_1 = data & 0x00FF0000; d_1 = d_1 >> 16; 
            unsigned int d_2 = data & 0x0000FF00; d_2 = d_2 >> 8;
            unsigned int d_3 = data & 0x000000FF; 
            d.push_back(d_0);
            d.push_back(d_1);
            d.push_back(d_2);
            d.push_back(d_3);
            
        };
        void add2bite(const int data ){
            unsigned int d_0 = data & 0x0000FF00; d_0 = d_0 >> 8; 
            unsigned int d_1 = data & 0x000000FF; 
            d.push_back(d_0);
            d.push_back(d_1);
        };

        void add(const int data ){
            unsigned int d_0 = data & 0x000000FF; 
            d.push_back(d_0);
        };
        int read4bite(const size_t i){
            if(d.size() < i+4)return 0;
            int d_0 = d[i]   << 24;
            int d_1 = d[i+1] << 16;
            int d_2 = d[i+2] << 8;
            int d_3 = d[i+3];
            int r_d{0};
            r_d += d_0;
            r_d += d_1;
            r_d += d_2;
            r_d += d_3;
            return r_d;
        };

        int read2bite(const size_t i){
            if(d.size() < i+2)return 0;
            int d_0 = d[i]   << 8;
            int d_1 = d[i+1];
            int r_d{0};
            r_d += d_0;
            r_d += d_1;
            return r_d;
        };
        int read(const size_t i){
            if(d.size() < i)return 0;
            int d_0 = d[i];
            int r_d = d_0;
            return r_d;
        };




    };*/
};
}



//modobus reference
//https://www.orientalmotor.co.jp/file/pdf/products/HM-60252J.pdf
namespace
{
    namespace MODBUS
    {

        inline void setup()
        {
/////230400
            Serial1.begin( DATA::MODBUS::boudrate , SERIAL_8E1);///parity mode
            Serial1.transmitterEnable(DATA::MODBUS::PIN_ENABLE);

            delay(1000);
        };

        inline void parse()
        {
            int incomingByte;
            while (Serial1.available())
            {
                incomingByte = Serial1.read();
                //Serial.print(incomingByte, HEX);
                //Serial.print(" ");
            }
            Serial1.flush();
            //Serial.println(incomingByte, HEX);
        };

        template <typename T>
        inline void send(const vector<T> &data){
            vector<T> _data = CRC::add(data);
            
            for(const auto &e : _data) { Serial1.write(e);} 
            
            /*
            Serial.println("--------------------------------");

            Serial.print("sendMess: ");
            for(const auto &e : _data) {             
                Serial.print(e,HEX);
                Serial.print(" ");
                } 
            Serial.println();
            */
            parse();
            delay(10);
        };

       template <typename T>
       vector<T>  set4bite(const int data , vector<T> &d){
            vector<T> _d = d;
            unsigned int d_0 = data & 0xFF000000; d_0 = d_0 >> 24; 
            unsigned int d_1 = data & 0x00FF0000; d_1 = d_1 >> 16; 
            unsigned int d_2 = data & 0x0000FF00; d_2 = d_2 >> 8;
            unsigned int d_3 = data & 0x000000FF; 
            _d.push_back(d_0);
            _d.push_back(d_1);
            _d.push_back(d_2);
            _d.push_back(d_3);
            //cout<<hex<<d[0]<<endl;
            //cout<<hex<<d[1]<<endl;
            //cout<<hex<<d[2]<<endl;
            //cout<<hex<<d[3]<<endl;
            return _d;
        };
        
       template <typename T>
       vector<T> set2bite(const int data ,vector<T> &d){
            vector<T> _d = d;
            unsigned int d_0 = data & 0x0000FF00; d_0 = d_0 >> 8; 
            unsigned int d_1 = data & 0x000000FF; 
            _d.push_back(d_0);
            _d.push_back(d_1);
            return _d;
        };
        int back4bite(int d_0,int d_1,int d_2,int d_3){
            d_0 = d_0 << 24;
            d_1 = d_1 << 16;
            d_2 = d_2 << 8;
            int d{0};
            d += d_0;
            d += d_1;
            d += d_2;
            d += d_3;
            return d;
        };

        int back2bite(int d_0,int d_1){
            d_0 = d_0 << 8;
            int d{0};
            d += d_0;
            d += d_1;
            return d;
        };


        inline void directDataDrive(const int ID,const int pos,const int spd,const int acc,const int dec){
            vector<int> d;
            d.push_back ( ID );
            d.push_back ( MODBUS::FUNCTION::WRITE_MULTIPLE );
            d = set2bite( 0x58 , d);  //// address
            d = set2bite( 0x10 , d);  //// num_resister
            d.push_back ( 0x20);      //// num_byte
            d = set4bite( 0x00 , d);  //// drive data num
            d = set4bite( 0x01 , d);  //// drive absolute 01 relative 02 
            d = set4bite( pos  , d);  //// pos
            d = set4bite( spd  , d);  //// spd
            d = set4bite( acc  , d);  //// acc
            d = set4bite( dec  , d);  //// dec
            d = set4bite( 0x3E8, d);  //// current %
            d = set4bite( 0x01 , d);  //// triger type 
            send(d);

        };

        inline void resetAlarm(const int ID){
            vector<int> d;
            d.push_back ( ID );
            d.push_back ( MODBUS::FUNCTION::WRITE_SINGLE );
            d = set2bite ( 0x180 ,d);  //// address 0
            d = set2bite ( 0x181 ,d);  //// address 1
            d = set2bite ( 0x00  ,d);  //// set 1st
            d = set2bite ( 0x02  ,d);  //// set 2nd 2
            send(d);

        };

        inline void resetPosition(const int ID){
            vector<int> d;
            d.push_back ( ID );
            d.push_back ( MODBUS::FUNCTION::WRITE_SINGLE );
            d = set2bite ( 0x18A ,d);  //// address 0
            d = set2bite ( 0x18B ,d);  //// address 1
            d = set2bite ( 0x00  ,d);  //// set 1st
            d = set2bite ( 0x02  ,d);  //// set 2nd 2
            send(d);

        };

        

        inline void read(const int ID,const int address,const int num_resiter){
            vector<int> d;
            d.push_back ( ID   );
            d.push_back ( MODBUS::FUNCTION::READ );
            d = set2bite( address     , d);
            d = set2bite( num_resiter , d);
            send(d);

        };


        inline void readCurrentPos(const int ID){
            read(ID,ADDRESS::CUR_POS,0x0006);
            vector<int> d;
            while (Serial1.available())
            {
                d.push_back (Serial1.read());
                DATA::MODBUS::STATE::count = 0;
            }
            Serial1.flush();
            if(d.size()<14){
                     return;
            }
            DATA::RECENT::pos   = back4bite(d[3], d[4], d[5], d[6]);
            DATA::RECENT::spd   = back4bite(d[11],d[12],d[13],d[14]);
        };

        inline void readAlarm(const int ID){
            read(ID,ADDRESS::CUR_ALRM,0x0002);
            vector<int> d;
            while (Serial1.available())
            {
                d.push_back (Serial1.read());
            }
            Serial1.flush();
            if(d.size()<7){           
                DATA::MODBUS::STATE::count ++;
                if(DATA::MODBUS::STATE::count > DATA::MODBUS::STATE::max)DATA::MODBUS::STATE::count = DATA::MODBUS::STATE::max;
                return;

            }
            DATA::RECENT::alarm  = back4bite(d[3], d[4], d[5], d[6]);

        };


    };
};




 /*
        inline void sendData(int data[])
        {

            for (int i = 0; i < 13; i++)
            {
                Serial2.write(data[i]);

                //Serial.print(data[i],HEX);
                // Serial.print(" ");
            };
        };
*/