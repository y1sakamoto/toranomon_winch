#pragma once
#include "config.h"
#include <ArduinoOSC.h>

namespace
{
    namespace DATA
    {

        namespace TEST
        {

            vector<int> d_0{0x01, 0x10, 0x18, 0x00, 0x00, 0x02, 0x04, 0x00, 0x00, 0x00, 0x02}; //,0xD8, 0x6E};
            vector<int> d_1{0x01, 0x10, 0x18, 0x02, 0x00, 0x02, 0x04, 0x00, 0x00, 0x21, 0x34}; //,0xC1, 0xF1};
            vector<int> d_2{0x01, 0x10, 0x18, 0x04, 0x00, 0x02, 0x04, 0x00, 0x00, 0x07, 0xD0}; //,0x5B, 0xF0};
            vector<int> d_3{0x01, 0x10, 0x18, 0x06, 0x00, 0x02, 0x04, 0x00, 0x00, 0x05, 0xDC}; //,0xDB, 0x4C};
            vector<int> d_4{0x01, 0x10, 0x18, 0x08, 0x00, 0x02, 0x04, 0x00, 0x00, 0x05, 0xDC}; //,0x5A, 0xC0};

            vector<int> d_5{0x01, 0x10, 0x00, 0x7C, 0x00, 0x02, 0x04, 0x00, 0x00, 0x00, 0x08}; //,0xF5, 0x18};
            vector<int> d_6{0x01, 0x10, 0x00, 0x7C, 0x00, 0x02, 0x04, 0x00, 0x50, 0xC2, 0x97}; //,0xF4, 0xDE};

        };

        namespace MODBUS
        {
            constexpr int PIN_ENABLE{2};
            //constexpr uint32_t boudrate{115200}; //switch 4
            constexpr int boudrate{230400}; //switch 5

            namespace STATE{
                int count{0};
                constexpr int threshhold {5};
                int max{5000};
            }; 

 

        };
        namespace PIN_ASSIGN
        {
            constexpr int IO_0{15};
            constexpr int IO_1{16};
            constexpr int IO_2{17};
            constexpr int IO_3{18};
            constexpr int IO_4{19};
            constexpr int IO_5{20};

        };

        namespace RECENT
        {
            //int type{0};
            int pos{0};
            int spd{0};
            int alarm{0};
        };
        namespace TARGET
        {
            int pos{0};
            int spd{0};
            int acc{0};
        };




        namespace OSC
        {
            int id{0};
            /*
            uint8_t mac[6]; 
            constexpr uint8_t mac_0[] = {0x00, 0x51, 0xC2, 0x97, 0x2E, 0x52};
            constexpr uint8_t mac_1[] = {0x00, 0x51, 0xC2, 0x97, 0x2F, 0x8B};
            constexpr uint8_t mac_2[] = {0x00, 0x51, 0xC2, 0x97, 0x2F, 0xA0};
            constexpr uint8_t mac_3[] = {0x00, 0x51, 0xC2, 0x97, 0x2F, 0x8D};
            constexpr uint8_t mac_4[] = {0x00, 0x51, 0xC2, 0x97, 0x2E, 0x6F};
            constexpr uint8_t mac_5[] = {0x00, 0x51, 0xC2, 0x97, 0x2F, 0xA7};
            constexpr uint8_t mac_6[] = {0x00, 0x51, 0xC2, 0x97, 0x2F, 0xA0};
            constexpr uint8_t mac_7[] = {0x00, 0x51, 0xC2, 0x97, 0x2F, 0x8D};
            constexpr uint8_t mac_8[] = {0x00, 0x51, 0xC2, 0x97, 0x2E, 0x6F};
            constexpr uint8_t mac_9[] = {0x00, 0x51, 0xC2, 0x97, 0x2F, 0xA7};

            constexpr uint8_t mac_10[] = {0x00, 0x50, 0xC2, 0x98, 0x2E, 0x56};
            constexpr uint8_t mac_11[] = {0x00, 0x50, 0xC2, 0x98, 0x2F, 0x8B};
            constexpr uint8_t mac_12[] = {0x00, 0x50, 0xC2, 0x98, 0x2F, 0xA0};
            constexpr uint8_t mac_13[] = {0x00, 0x50, 0xC2, 0x98, 0x2F, 0x8D};
            constexpr uint8_t mac_14[] = {0x00, 0x50, 0xC2, 0x98, 0x2E, 0x6F};
            constexpr uint8_t mac_15[] = {0x00, 0x50, 0xC2, 0x98, 0x2F, 0xA7};
            constexpr uint8_t mac_16[] = {0x00, 0x50, 0xC2, 0x98, 0x2F, 0xA0};
            constexpr uint8_t mac_17[] = {0x00, 0x50, 0xC2, 0x98, 0x2F, 0x8D};
            constexpr uint8_t mac_18[] = {0x00, 0x50, 0xC2, 0x98, 0x2E, 0x6F};
            constexpr uint8_t mac_19[] = {0x00, 0x50, 0xC2, 0x98, 0x2F, 0xA7};

            constexpr uint8_t mac_20[] = {0x00, 0x50, 0xC2, 0x99, 0x2E, 0x56};
            constexpr uint8_t mac_21[] = {0x00, 0x50, 0xC2, 0x99, 0x2F, 0x8B};
            constexpr uint8_t mac_22[] = {0x00, 0x50, 0xC2, 0x99, 0x2F, 0xA0};
            constexpr uint8_t mac_23[] = {0x00, 0x50, 0xC2, 0x99, 0x2F, 0x8D};
            constexpr uint8_t mac_24[] = {0x00, 0x50, 0xC2, 0x99, 0x2E, 0x6F};
            constexpr uint8_t mac_25[] = {0x00, 0x50, 0xC2, 0x99, 0x2F, 0xA7};
            constexpr uint8_t mac_26[] = {0x00, 0x50, 0xC2, 0x99, 0x2F, 0xA0};
            constexpr uint8_t mac_27[] = {0x00, 0x50, 0xC2, 0x99, 0x2F, 0x8D};
            constexpr uint8_t mac_28[] = {0x00, 0x50, 0xC2, 0x99, 0x2E, 0x6F};
            constexpr uint8_t mac_29[] = {0x00, 0x50, 0xC2, 0x99, 0x2F, 0xA7};

            constexpr uint8_t mac_30[] = {0x00, 0x50, 0xC2, 0x17, 0x2F, 0xA7};
    */
            /////uint8_t mac[] = {0x00, 0x50, 0xC2, 0x97, 0x2E, 0xE8};

            /*
            /////////////NOMAL///////////////
            IPAddress ip(192, 168, 1, 200 + id);
            //uint8_t mac[] = {0x00, 0x50, 0xC2, 0x97, 0x2E, 0xE8};
            const char *host = "192.168.1.20";
            ////////////NOMAL/////////////
            */


           ///////2023 TORANOMON/////////
            IPAddress ip(192, 168, 0, 170 + id);

            //const IPAddress ip(192, 168, 1, 201);

            uint8_t mac[] = {0x00, 0x51, 0xC2, 0x97, 0x2E, 0x00};
            const char *host = "192.168.0.31";
           ///////2023 TORANOMON/////////


            const int recv_port = 54322;
            //const int bind_port = 54345;
            const int send_port = 55556;
           // const int publish_port = 54445;

        };

    };
};