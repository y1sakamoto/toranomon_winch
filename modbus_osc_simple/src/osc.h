#include <ArduinoOSC.h>
#include "data.h"
#include "modbus.h"
namespace OSC{
    inline void init(){
        /*
        ////////**********NOMAL***************..//////////////
        const IPAddress ip(192, 168, 1, 200 + DATA::OSC::id);
        DATA::OSC::ip = ip;
        Ethernet.begin(DATA::OSC::mac, DATA::OSC::ip);
        ////////**********NOMAL***************..//////////////

        */
        ////////**********2023 TORANOMON***************..//////////////
        const IPAddress ip(192, 168, 0, 170 + DATA::OSC::id);
        DATA::OSC::ip = ip;
        //DATA::OSC::mac[5] += DATA::OSC::id;
        Ethernet.begin(DATA::OSC::mac, DATA::OSC::ip);
        ////////**********2023 TORANOMON***************..//////////////

        //Ethernet.begin(DATA::OSC::ip);

        //OscEther.publish(DATA::OSC::host, DATA::OSC::send_port, "/publish/value", 10)
        
 /*
        OscEther.publish(DATA::OSC::host, DATA::OSC::send_port, "/status", DATA::OSC::id, DATA::RECENT::pos, DATA::RECENT::spd)
                    ->setIntervalMsec(30.f);
        OscEther.publish(DATA::OSC::host, DATA::OSC::send_port, "/alarm" , DATA::OSC::id, DATA::RECENT::alarm)                    
                    ->setIntervalMsec(30.f);
*/

        //->setFrameRate(60.f);
        OscEther.subscribe(DATA::OSC::recv_port, "/ping", []() {
            OscEther.send(DATA::OSC::host, DATA::OSC::send_port, "/reply", DATA::OSC::id);

        });
        /*
        OscEther.subscribe(DATA::OSC::recv_port, "/run",[](OscMessage& m) {
            const int p{m.arg<int>(0)};
            const int s{m.arg<int>(1)};
            const int a{m.arg<int>(2)};
            const int d{m.arg<int>(3)};

            MODBUS::directDataDrive(1,p,s,a,d);
            OscEther.send(DATA::OSC::host, DATA::OSC::send_port, "/run", DATA::OSC::id,p,s,a,d);

        });*/

        
        OscEther.subscribe(DATA::OSC::recv_port, "/run",[](OscMessage& m) {
            DATA::TARGET::pos = m.arg<int>(0);
            DATA::TARGET::spd = m.arg<int>(1);
            DATA::TARGET::acc = m.arg<int>(2);
            MODBUS::directDataDrive(1,DATA::TARGET::pos , DATA::TARGET::spd , DATA::TARGET::acc , DATA::TARGET::acc);
            OscEther.send(DATA::OSC::host, DATA::OSC::send_port, "/run", DATA::OSC::id, DATA::TARGET::pos, DATA::TARGET::spd);
        });

        OscEther.subscribe(DATA::OSC::recv_port, "/reset/alarm", []() {
            MODBUS::resetAlarm(1);
            OscEther.send(DATA::OSC::host, DATA::OSC::send_port, "/reset/alarm", DATA::OSC::id);
        });

        OscEther.subscribe(DATA::OSC::recv_port, "/reset/position", []() {
            MODBUS::resetPosition(1);
            OscEther.send(DATA::OSC::host, DATA::OSC::send_port, "/reset/position", DATA::OSC::id);
        });
        
        OscEther.subscribe(DATA::OSC::recv_port, "/modbus/state/reset", []() {
            DATA::MODBUS::STATE::count = 0;
        });
    };

    inline void update(){
        OscEther.update(); 
        OscEther.send(DATA::OSC::host, DATA::OSC::send_port, "/status",       DATA::OSC::id, DATA::RECENT::pos, DATA::RECENT::spd   );
        OscEther.send(DATA::OSC::host, DATA::OSC::send_port, "/modbus/state", DATA::OSC::id, DATA::MODBUS::STATE::count             );
        OscEther.send(DATA::OSC::host, DATA::OSC::send_port, "/alarm" , DATA::OSC::id, DATA::RECENT::alarm);
    };
};