<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.6.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="58" name="bCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="yes" active="yes"/>
<layer number="101" name="Patch_Top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="centerline" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="no"/>
<layer number="110" name="110" color="7" fill="1" visible="no" active="no"/>
<layer number="111" name="111" color="7" fill="1" visible="no" active="no"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="no"/>
<layer number="113" name="ReferenceLS" color="7" fill="1" visible="no" active="no"/>
<layer number="114" name="Panel" color="11" fill="1" visible="yes" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="no" active="yes"/>
<layer number="116" name="Patch_BOT" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="no"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="no"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="prix" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="test" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="no"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="218" name="218bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="219" name="219bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="220" name="220bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="221" name="221bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="222" name="222bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="223" name="223bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="224" name="224bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="no"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="no"/>
<layer number="250" name="Descript" color="7" fill="1" visible="yes" active="yes"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="yes" active="yes"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="WIZ850IO">
<packages>
<package name="WIZ850IO">
<description>&lt;b&gt;WIZ850io-7&lt;/b&gt;&lt;br&gt;
</description>
<text x="10.16" y="-6.1" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="10.16" y="-6.1" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-1.34" y1="6.4" x2="21.66" y2="6.4" width="0.2" layer="51"/>
<wire x1="21.66" y1="6.4" x2="21.66" y2="-18.6" width="0.2" layer="51"/>
<wire x1="21.66" y1="-18.6" x2="-1.34" y2="-18.6" width="0.2" layer="51"/>
<wire x1="-1.34" y1="-18.6" x2="-1.34" y2="6.4" width="0.2" layer="51"/>
<wire x1="-1.34" y1="6.4" x2="21.66" y2="6.4" width="0.1" layer="21"/>
<wire x1="21.66" y1="6.4" x2="21.66" y2="-18.6" width="0.1" layer="21"/>
<wire x1="21.66" y1="-18.6" x2="-1.34" y2="-18.6" width="0.1" layer="21"/>
<wire x1="-1.34" y1="-18.6" x2="-1.34" y2="6.4" width="0.1" layer="21"/>
<wire x1="-2.34" y1="7.4" x2="22.66" y2="7.4" width="0.1" layer="51"/>
<wire x1="22.66" y1="7.4" x2="22.66" y2="-19.6" width="0.1" layer="51"/>
<wire x1="22.66" y1="-19.6" x2="-2.34" y2="-19.6" width="0.1" layer="51"/>
<wire x1="-2.34" y1="-19.6" x2="-2.34" y2="7.4" width="0.1" layer="51"/>
<pad name="1" x="0" y="0" drill="1.11" diameter="1.665" shape="square"/>
<pad name="2" x="0" y="-2.54" drill="1.11" diameter="1.665"/>
<pad name="3" x="0" y="-5.08" drill="1.11" diameter="1.665"/>
<pad name="4" x="0" y="-7.62" drill="1.11" diameter="1.665"/>
<pad name="5" x="0" y="-10.16" drill="1.11" diameter="1.665"/>
<pad name="6" x="0" y="-12.7" drill="1.11" diameter="1.665"/>
<pad name="7" x="20.32" y="0" drill="1.11" diameter="1.665" shape="square"/>
<pad name="8" x="20.32" y="-2.54" drill="1.11" diameter="1.665"/>
<pad name="9" x="20.32" y="-5.08" drill="1.11" diameter="1.665"/>
<pad name="10" x="20.32" y="-7.62" drill="1.11" diameter="1.665"/>
<pad name="11" x="20.32" y="-10.16" drill="1.11" diameter="1.665"/>
<pad name="12" x="20.32" y="-12.7" drill="1.11" diameter="1.665"/>
</package>
</packages>
<symbols>
<symbol name="WIZ850IO">
<wire x1="5.08" y1="2.54" x2="27.94" y2="2.54" width="0.254" layer="94"/>
<wire x1="27.94" y1="-15.24" x2="27.94" y2="2.54" width="0.254" layer="94"/>
<wire x1="27.94" y1="-15.24" x2="5.08" y2="-15.24" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-15.24" width="0.254" layer="94"/>
<text x="29.21" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="29.21" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="GND_1" x="0" y="0" length="middle"/>
<pin name="GND_2" x="0" y="-2.54" length="middle"/>
<pin name="MOSI" x="0" y="-5.08" length="middle"/>
<pin name="SCLK" x="0" y="-7.62" length="middle"/>
<pin name="SCNN" x="0" y="-10.16" length="middle"/>
<pin name="INTN" x="0" y="-12.7" length="middle"/>
<pin name="GND_3" x="33.02" y="0" length="middle" rot="R180"/>
<pin name="3.3V_1" x="33.02" y="-2.54" length="middle" rot="R180"/>
<pin name="3.3V_2" x="33.02" y="-5.08" length="middle" rot="R180"/>
<pin name="NC" x="33.02" y="-7.62" length="middle" direction="nc" rot="R180"/>
<pin name="RSTN" x="33.02" y="-10.16" length="middle" rot="R180"/>
<pin name="MISO" x="33.02" y="-12.7" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="WIZ850IO" prefix="IC">
<description>&lt;b&gt;Networking Modules W5500 + MAG Jack ioPlatform Mod&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://wizwiki.net/wiki/doku.php?id=products:wiz850io:start#hardware_specification"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="WIZ850IO" x="0" y="0"/>
</gates>
<devices>
<device name="" package="WIZ850IO">
<connects>
<connect gate="G$1" pin="3.3V_1" pad="8"/>
<connect gate="G$1" pin="3.3V_2" pad="9"/>
<connect gate="G$1" pin="GND_1" pad="1"/>
<connect gate="G$1" pin="GND_2" pad="2"/>
<connect gate="G$1" pin="GND_3" pad="7"/>
<connect gate="G$1" pin="INTN" pad="6"/>
<connect gate="G$1" pin="MISO" pad="12"/>
<connect gate="G$1" pin="MOSI" pad="3"/>
<connect gate="G$1" pin="NC" pad="10"/>
<connect gate="G$1" pin="RSTN" pad="11"/>
<connect gate="G$1" pin="SCLK" pad="4"/>
<connect gate="G$1" pin="SCNN" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="Unavailable"/>
<attribute name="DESCRIPTION" value=" Ethernet 10/100 Base-T/TX PHY SPI Interface Module "/>
<attribute name="MF" value="WIZnet"/>
<attribute name="MP" value="WIZ850IO"/>
<attribute name="PACKAGE" value="Module WIZnet"/>
<attribute name="PRICE" value="None"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-PowerSymbols" urn="urn:adsk.eagle:library:530">
<description>&lt;h3&gt;SparkFun Power Symbols&lt;/h3&gt;
This library contains power, ground, and voltage-supply symbols.
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
</packages>
<symbols>
<symbol name="DGND" urn="urn:adsk.eagle:symbol:39415/1" library_version="1">
<description>&lt;h3&gt;Digital Ground Supply&lt;/h3&gt;</description>
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
<text x="0" y="-0.254" size="1.778" layer="96" align="top-center">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:39439/1" prefix="GND" library_version="1">
<description>&lt;h3&gt;Ground Supply Symbol&lt;/h3&gt;
&lt;p&gt;Generic signal ground supply symbol.&lt;/p&gt;</description>
<gates>
<gate name="1" symbol="DGND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Power_Symbols">
<description>&lt;B&gt;Supply &amp; Ground symbols</description>
<packages>
</packages>
<symbols>
<symbol name="+3.3V" urn="urn:adsk.eagle:symbol:18498252/2">
<description>3.3 Volt (3V3) Bar</description>
<wire x1="1.905" y1="2.54" x2="-1.905" y2="2.54" width="0.254" layer="94"/>
<text x="-0.127" y="3.048" size="1.778" layer="96" align="bottom-center">&gt;VALUE</text>
<pin name="+3.3V" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="+5V" urn="urn:adsk.eagle:symbol:18498226/2">
<description>5 Volt (5V0) Bar</description>
<wire x1="1.905" y1="2.54" x2="-1.905" y2="2.54" width="0.254" layer="94"/>
<text x="-0.127" y="3.048" size="1.778" layer="96" align="bottom-center">&gt;VALUE</text>
<pin name="+5V" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="+V" urn="urn:adsk.eagle:symbol:16502354/4">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="0" y="0.635" size="1.778" layer="96" align="bottom-center">&gt;VALUE</text>
<pin name="+V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="-V" urn="urn:adsk.eagle:symbol:16502353/4">
<wire x1="1.905" y1="2.54" x2="-1.905" y2="2.54" width="0.254" layer="94"/>
<text x="0" y="3.175" size="1.778" layer="96" align="bottom-center">&gt;VALUE</text>
<pin name="-V" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+3.3V" urn="urn:adsk.eagle:component:16502431/6" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;  3.3 Volt (3V3) Bar</description>
<gates>
<gate name="G$1" symbol="+3.3V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name="">
<attribute name="CATEGORY" value="Supply" constant="no"/>
<attribute name="VALUE" value="+3.3V" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V" urn="urn:adsk.eagle:component:16502432/6" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;  5 Volt (5V0) Bar</description>
<gates>
<gate name="G$1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name="">
<attribute name="CATEGORY" value="Supply" constant="no"/>
<attribute name="VALUE" value="+5V" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+V" urn="urn:adsk.eagle:component:16502422/6" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;  Positive Voltage (+V) Bar</description>
<gates>
<gate name="G$1" symbol="+V" x="0" y="2.54"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name="">
<attribute name="CATEGORY" value="Supply" constant="no"/>
<attribute name="VALUE" value="+V" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="-V" urn="urn:adsk.eagle:component:16502423/6" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;  Negative Voltage (-V) Bar</description>
<gates>
<gate name="G$1" symbol="-V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name="">
<attribute name="CATEGORY" value="Supply" constant="no"/>
<attribute name="VALUE" value="-V" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="CRE1S2405SC">
<description>&lt;Isolated 1W Single Output Isolated DC/DC Converters&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="CRE1(SC)">
<description>&lt;b&gt;CRE1(SC)&lt;/b&gt;&lt;br&gt;
</description>
<pad name="1" x="-3.81" y="1.13" drill="0.83" diameter="1.33"/>
<pad name="2" x="-1.27" y="1.13" drill="0.83" diameter="1.33"/>
<pad name="3" x="1.27" y="1.13" drill="0.83" diameter="1.33"/>
<pad name="4" x="3.81" y="1.13" drill="0.83" diameter="1.33"/>
<text x="-0.475" y="4.007" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="-0.475" y="4.007" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-5.765" y1="6.1" x2="5.765" y2="6.1" width="0.2" layer="51"/>
<wire x1="5.765" y1="6.1" x2="5.765" y2="0" width="0.2" layer="51"/>
<wire x1="5.765" y1="0" x2="-5.765" y2="0" width="0.2" layer="51"/>
<wire x1="-5.765" y1="0" x2="-5.765" y2="6.1" width="0.2" layer="51"/>
<wire x1="-5.765" y1="0" x2="-5.765" y2="6.1" width="0.2" layer="21"/>
<wire x1="-5.765" y1="6.1" x2="5.765" y2="6.1" width="0.2" layer="21"/>
<wire x1="5.765" y1="6.1" x2="5.765" y2="0" width="0.2" layer="21"/>
<wire x1="5.765" y1="0" x2="-5.765" y2="0" width="0.2" layer="21"/>
<circle x="-3.827" y="-0.776" radius="0.0508" width="0.2" layer="25"/>
</package>
</packages>
<symbols>
<symbol name="CRE1S2405SC">
<wire x1="5.08" y1="2.54" x2="20.32" y2="2.54" width="0.254" layer="94"/>
<wire x1="20.32" y1="-10.16" x2="20.32" y2="2.54" width="0.254" layer="94"/>
<wire x1="20.32" y1="-10.16" x2="5.08" y2="-10.16" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-10.16" width="0.254" layer="94"/>
<text x="21.59" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="21.59" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="-VIN" x="0" y="0" length="middle"/>
<pin name="+VIN" x="0" y="-2.54" length="middle"/>
<pin name="-VOUT" x="0" y="-5.08" length="middle"/>
<pin name="+VOUT" x="0" y="-7.62" length="middle"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CRE1S2405SC" prefix="U">
<description>&lt;b&gt;Isolated 1W Single Output Isolated DC/DC Converters&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://power.murata.com/datasheet?/data/power/ncl/kdc_cre1.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="CRE1S2405SC" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CRE1(SC)">
<connects>
<connect gate="G$1" pin="+VIN" pad="2"/>
<connect gate="G$1" pin="+VOUT" pad="4"/>
<connect gate="G$1" pin="-VIN" pad="1"/>
<connect gate="G$1" pin="-VOUT" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="ARROW_PART_NUMBER" value="CRE1S2405SC" constant="no"/>
<attribute name="ARROW_PRICE-STOCK" value="https://www.arrow.com/en/products/cre1s2405sc/murata-power-solutions" constant="no"/>
<attribute name="DESCRIPTION" value="Isolated 1W Single Output Isolated DC/DC Converters" constant="no"/>
<attribute name="HEIGHT" value="mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Murata Electronics" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="CRE1S2405SC" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="580-CRE1S2405SC" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="https://www.mouser.co.uk/ProductDetail/Murata-Power-Solutions/CRE1S2405SC?qs=pFP7BvE1PmwUsyXD%252BXcXxQ%3D%3D" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun">
<packages>
<package name="SO08">
<description>&lt;b&gt;Small Outline Package&lt;/b&gt; Fits JEDEC packages (narrow SOIC-8)</description>
<wire x1="-2.362" y1="-1.803" x2="2.362" y2="-1.803" width="0.1524" layer="51"/>
<wire x1="2.362" y1="-1.803" x2="2.362" y2="1.803" width="0.1524" layer="21"/>
<wire x1="2.362" y1="1.803" x2="-2.362" y2="1.803" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="1.803" x2="-2.362" y2="-1.803" width="0.1524" layer="21"/>
<circle x="-1.8034" y="-0.9906" radius="0.1436" width="0.2032" layer="21"/>
<smd name="1" x="-1.905" y="-2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="2" x="-0.635" y="-2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="3" x="0.635" y="-2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="4" x="1.905" y="-2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="5" x="1.905" y="2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="6" x="0.635" y="2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="7" x="-0.635" y="2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="8" x="-1.905" y="2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<text x="-1.27" y="-0.635" size="0.4064" layer="27">&gt;VALUE</text>
<text x="-1.27" y="0" size="0.4064" layer="25">&gt;NAME</text>
<rectangle x1="-2.0828" y1="-2.8702" x2="-1.7272" y2="-1.8542" layer="51"/>
<rectangle x1="-0.8128" y1="-2.8702" x2="-0.4572" y2="-1.8542" layer="51"/>
<rectangle x1="0.4572" y1="-2.8702" x2="0.8128" y2="-1.8542" layer="51"/>
<rectangle x1="1.7272" y1="-2.8702" x2="2.0828" y2="-1.8542" layer="51"/>
<rectangle x1="-2.0828" y1="1.8542" x2="-1.7272" y2="2.8702" layer="51"/>
<rectangle x1="-0.8128" y1="1.8542" x2="-0.4572" y2="2.8702" layer="51"/>
<rectangle x1="0.4572" y1="1.8542" x2="0.8128" y2="2.8702" layer="51"/>
<rectangle x1="1.7272" y1="1.8542" x2="2.0828" y2="2.8702" layer="51"/>
</package>
<package name="1X03">
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.635" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="MOLEX-1X3">
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="6.35" y1="3.048" x2="6.35" y2="-2.54" width="0.127" layer="21"/>
<wire x1="6.35" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="6.35" y1="-2.54" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="5.08" y2="-1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
</package>
<package name="SCREWTERMINAL-3.5MM-3">
<wire x1="-2.3" y1="3.4" x2="9.3" y2="3.4" width="0.2032" layer="21"/>
<wire x1="9.3" y1="3.4" x2="9.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="9.3" y1="-2.8" x2="9.3" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="9.3" y1="-3.6" x2="-2.3" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-3.6" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-2.8" x2="-2.3" y2="3.4" width="0.2032" layer="21"/>
<wire x1="9.3" y1="-2.8" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.35" x2="-2.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-1.35" x2="-2.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-2.35" x2="-2.3" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="9.3" y1="3.15" x2="9.7" y2="3.15" width="0.2032" layer="51"/>
<wire x1="9.7" y1="3.15" x2="9.7" y2="2.15" width="0.2032" layer="51"/>
<wire x1="9.7" y1="2.15" x2="9.3" y2="2.15" width="0.2032" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<pad name="3" x="7" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X03_LOCK">
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.635" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X03_LOCK_LONGPADS">
<description>This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place.  
You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  
This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" 
to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers.
Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,
if you push a header all the way into place, it is covered up entirely on the bottom side.  This idea of altering the position of holes to aid alignment 
will be further integrated into the Sparkfun Library for other footprints.  It can help hold any component with 3 or more connection pins.</description>
<wire x1="1.524" y1="-0.127" x2="1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="4.064" y1="-0.127" x2="3.556" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.016" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.8636" x2="-0.9906" y2="1.143" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.1176" x2="-0.9906" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.127" x2="6.096" y2="-0.127" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.127" x2="6.35" y2="-1.1176" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-1.1176" x2="6.0706" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.127" x2="6.35" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.8636" x2="6.0706" y2="1.143" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="-0.254" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-1.27" y="1.778" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-3.302" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.4191" x2="0.2921" y2="0.1651" layer="51"/>
<rectangle x1="2.2479" y1="-0.4191" x2="2.8321" y2="0.1651" layer="51"/>
<rectangle x1="4.7879" y1="-0.4191" x2="5.3721" y2="0.1651" layer="51"/>
</package>
<package name="MOLEX-1X3_LOCK">
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="6.35" y1="3.048" x2="6.35" y2="-2.54" width="0.127" layer="21"/>
<wire x1="6.35" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="6.35" y1="-2.54" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="5.08" y2="-1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796"/>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
<rectangle x1="4.7879" y1="-0.2921" x2="5.3721" y2="0.2921" layer="51"/>
</package>
<package name="SCREWTERMINAL-3.5MM-3_LOCK.007S">
<wire x1="-2.3" y1="3.4" x2="9.3" y2="3.4" width="0.2032" layer="21"/>
<wire x1="9.3" y1="3.4" x2="9.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="9.3" y1="-2.8" x2="9.3" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="9.3" y1="-3.6" x2="-2.3" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-3.6" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-2.8" x2="-2.3" y2="3.4" width="0.2032" layer="21"/>
<wire x1="9.3" y1="-2.8" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.35" x2="-2.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-1.35" x2="-2.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-2.35" x2="-2.3" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="9.3" y1="3.15" x2="9.7" y2="3.15" width="0.2032" layer="51"/>
<wire x1="9.7" y1="3.15" x2="9.7" y2="2.15" width="0.2032" layer="51"/>
<wire x1="9.7" y1="2.15" x2="9.3" y2="2.15" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="3.5" y="0" radius="0.425" width="0.001" layer="51"/>
<circle x="7" y="0" radius="0.425" width="0.001" layer="51"/>
<pad name="1" x="-0.1778" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<pad name="3" x="7.1778" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X03_NO_SILK">
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X03_LONGPADS">
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.635" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<text x="-1.3462" y="2.4638" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="JST-3-PTH">
<wire x1="-4" y1="-6.3" x2="-4" y2="1.5" width="0.2032" layer="21"/>
<wire x1="-4" y1="1.5" x2="4" y2="1.5" width="0.2032" layer="21"/>
<wire x1="4" y1="1.5" x2="4" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="-4" y1="-6.3" x2="-3.3" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="4" y1="-6.3" x2="3.3" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="-6.3" x2="-3.3" y2="-5" width="0.2032" layer="21"/>
<wire x1="3.3" y1="-6.3" x2="3.3" y2="-5" width="0.2032" layer="21"/>
<pad name="1" x="-2" y="-5" drill="0.7" diameter="1.4478"/>
<pad name="2" x="0" y="-5" drill="0.7" diameter="1.4478"/>
<pad name="3" x="2" y="-5" drill="0.7" diameter="1.4478"/>
<text x="-1.27" y="0.24" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="-1.03" size="0.4064" layer="27">&gt;Value</text>
<text x="-2.4" y="-4.33" size="1.27" layer="51">+</text>
<text x="-0.4" y="-4.33" size="1.27" layer="51">-</text>
<text x="1.7" y="-4.13" size="0.8" layer="51">S</text>
</package>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="RS485">
<wire x1="-7.62" y1="7.62" x2="12.7" y2="7.62" width="0.254" layer="94"/>
<wire x1="12.7" y1="7.62" x2="12.7" y2="0" width="0.254" layer="94"/>
<wire x1="12.7" y1="0" x2="12.7" y2="-2.54" width="0.254" layer="94"/>
<wire x1="12.7" y1="-2.54" x2="12.7" y2="-10.16" width="0.254" layer="94"/>
<wire x1="12.7" y1="-10.16" x2="-7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-10.16" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-2.54" x2="-7.62" y2="0" width="0.254" layer="94"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="2.54" width="0.254" layer="94"/>
<wire x1="-7.62" y1="2.54" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="3.556" width="0.254" layer="94"/>
<wire x1="2.54" y1="3.556" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="5.08" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="0" y2="-7.62" width="0.254" layer="94"/>
<wire x1="0" y1="-7.62" x2="1.27" y2="-6.35" width="0.254" layer="94"/>
<wire x1="1.27" y1="-6.35" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="0.762" y2="-3.302" width="0.254" layer="94"/>
<wire x1="0.762" y1="-3.302" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="-6.35" x2="1.27" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="5.08" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0.508" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-1.524" x2="0.762" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="0.762" y1="-1.524" x2="0.762" y2="-3.302" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-3.556" x2="3.81" y2="-3.556" width="0.1524" layer="94"/>
<wire x1="3.81" y1="-3.556" x2="3.81" y2="0" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0" x2="3.81" y2="1.27" width="0.1524" layer="94"/>
<wire x1="3.81" y1="1.27" x2="3.048" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="3.556" width="0.1524" layer="94"/>
<wire x1="5.08" y1="3.556" x2="2.54" y2="3.556" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="2.54" x2="-6.096" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="0" x2="-6.096" y2="0" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="-2.54" x2="-6.096" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-6.096" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="0" y1="-5.08" x2="-2.54" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="7.874" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0" x2="7.874" y2="0" width="0.1524" layer="94"/>
<wire x1="12.7" y1="0" x2="11.176" y2="0" width="0.1524" layer="94"/>
<wire x1="12.7" y1="-2.54" x2="11.176" y2="-2.54" width="0.1524" layer="94"/>
<circle x="1.27" y="0.762" radius="0.254" width="0.254" layer="94"/>
<circle x="2.794" y="1.27" radius="0.254" width="0.254" layer="94"/>
<circle x="1.524" y="-3.556" radius="0.254" width="0.254" layer="94"/>
<circle x="3.81" y="0" radius="0.127" width="0.1524" layer="94"/>
<circle x="5.08" y="-2.54" radius="0.127" width="0.1524" layer="94"/>
<text x="-7.62" y="8.128" size="1.778" layer="95">&gt;Name</text>
<text x="-7.62" y="-12.7" size="1.778" layer="96">&gt;Value</text>
<pin name="RO" x="-10.16" y="2.54" visible="pin" length="short"/>
<pin name="/RE" x="-10.16" y="0" visible="pin" length="short"/>
<pin name="DE" x="-10.16" y="-2.54" visible="pin" length="short"/>
<pin name="DI" x="-10.16" y="-5.08" visible="pin" length="short"/>
<pin name="VCC" x="15.24" y="2.54" visible="pin" length="short" rot="R180"/>
<pin name="B" x="15.24" y="0" visible="pin" length="short" rot="R180"/>
<pin name="A" x="15.24" y="-2.54" visible="pin" length="short" rot="R180"/>
<pin name="GND" x="15.24" y="-5.08" visible="pin" length="short" rot="R180"/>
</symbol>
<symbol name="M03">
<wire x1="3.81" y1="-5.08" x2="-2.54" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<text x="-2.54" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.54" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RS485">
<gates>
<gate name="G$1" symbol="RS485" x="-2.54" y="0"/>
</gates>
<devices>
<device name="SOIC" package="SO08">
<connects>
<connect gate="G$1" pin="/RE" pad="2"/>
<connect gate="G$1" pin="A" pad="6"/>
<connect gate="G$1" pin="B" pad="7"/>
<connect gate="G$1" pin="DE" pad="3"/>
<connect gate="G$1" pin="DI" pad="4"/>
<connect gate="G$1" pin="GND" pad="5"/>
<connect gate="G$1" pin="RO" pad="1"/>
<connect gate="G$1" pin="VCC" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="M03" prefix="JP" uservalue="yes">
<description>&lt;b&gt;Header 3&lt;/b&gt;
Standard 3-pin 0.1" header. Use with straight break away headers (SKU : PRT-00116), right angle break away headers (PRT-00553), swiss pins (PRT-00743), machine pins (PRT-00117), and female headers (PRT-00115). Molex polarized connector foot print use with SKU : PRT-08232 with associated crimp pins and housings.</description>
<gates>
<gate name="G$1" symbol="M03" x="-2.54" y="0"/>
</gates>
<devices>
<device name="PTH" package="1X03">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR" package="MOLEX-1X3">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SCREW" package="SCREWTERMINAL-3.5MM-3">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK" package="1X03_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X03_LOCK_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR_LOCK" package="MOLEX-1X3_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SCREW_LOCK" package="SCREWTERMINAL-3.5MM-3_LOCK.007S">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1X03_NO_SILK" package="1X03_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LONGPADS" package="1X03_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH" package="JST-3-PTH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Resistors">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find resistors, capacitors, inductors, test points, jumper pads, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="0603-RES">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="0.762" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.4064" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2286" y1="-0.381" x2="0.2286" y2="0.381" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="22OHM1/10W1%(0603)" prefix="R" uservalue="yes">
<description>RES-08698</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="0603-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-08698" constant="no"/>
<attribute name="VALUE" value="22" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="330OHM1/10W1%(0603)" prefix="R" uservalue="yes">
<description>RES-00818</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="0603-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-00818"/>
<attribute name="VALUE" value="330" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Capacitors">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find resistors, capacitors, inductors, test points, jumper pads, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="0603-CAP">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<wire x1="0" y1="0.0305" x2="0" y2="-0.0305" width="0.5588" layer="21"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="0.762" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.4064" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
</packages>
<symbols>
<symbol name="CAP">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="1.524" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="0.1UF-25V(+80/-20%)(0603)" prefix="C" uservalue="yes">
<description>CAP-00810&lt;br&gt;
Ceramic&lt;br&gt;
Standard decoupling cap</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="0603-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-00810"/>
<attribute name="VALUE" value="0.1uF" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="JC_Teensy356">
<packages>
<package name="TEENSY_3.5/3.6_BASIC">
<pad name="1/TX" x="-22.86" y="-7.62" drill="0.9652"/>
<pad name="2" x="-20.32" y="-7.62" drill="0.9652"/>
<pad name="3/CANTX" x="-17.78" y="-7.62" drill="0.9652"/>
<pad name="4/CANRX" x="-15.24" y="-7.62" drill="0.9652"/>
<pad name="5" x="-12.7" y="-7.62" drill="0.9652"/>
<pad name="6" x="-10.16" y="-7.62" drill="0.9652"/>
<pad name="7" x="-7.62" y="-7.62" drill="0.9652"/>
<pad name="8" x="-5.08" y="-7.62" drill="0.9652"/>
<pad name="9/CS" x="-2.54" y="-7.62" drill="0.9652"/>
<pad name="10/CS" x="0" y="-7.62" drill="0.9652"/>
<pad name="11/MOSI" x="2.54" y="-7.62" drill="0.9652"/>
<pad name="12/MISO" x="5.08" y="-7.62" drill="0.9652"/>
<pad name="13/SCK" x="5.08" y="7.62" drill="0.9652"/>
<pad name="14/A0" x="2.54" y="7.62" drill="0.9652"/>
<pad name="15/A1" x="0" y="7.62" drill="0.9652"/>
<pad name="16/A2" x="-2.54" y="7.62" drill="0.9652"/>
<pad name="17/A3" x="-5.08" y="7.62" drill="0.9652"/>
<pad name="18/A4/SDA" x="-7.62" y="7.62" drill="0.9652"/>
<pad name="19/A5/SCL" x="-10.16" y="7.62" drill="0.9652"/>
<pad name="20/A6" x="-12.7" y="7.62" drill="0.9652"/>
<pad name="21/A7" x="-15.24" y="7.62" drill="0.9652"/>
<pad name="22/A8" x="-17.78" y="7.62" drill="0.9652"/>
<pad name="23/A9" x="-20.32" y="7.62" drill="0.9652"/>
<pad name="0/RX" x="-25.4" y="-7.62" drill="0.9652"/>
<pad name="3V3_LO" x="-22.86" y="7.62" drill="0.9652"/>
<pad name="AGND" x="-25.4" y="7.62" drill="0.9652"/>
<pad name="GND" x="-27.94" y="-7.62" drill="0.9652"/>
<pad name="VIN" x="-27.94" y="7.62" drill="0.9652"/>
<pad name="3V3" x="7.62" y="-7.62" drill="0.9652"/>
<pad name="GND2" x="7.62" y="7.62" drill="0.9652"/>
<pad name="24" x="10.16" y="-7.62" drill="0.9652"/>
<pad name="25" x="12.7" y="-7.62" drill="0.9652"/>
<pad name="26" x="15.24" y="-7.62" drill="0.9652"/>
<pad name="27" x="17.78" y="-7.62" drill="0.9652"/>
<pad name="28" x="20.32" y="-7.62" drill="0.9652"/>
<pad name="29" x="22.86" y="-7.62" drill="0.9652"/>
<pad name="30" x="25.4" y="-7.62" drill="0.9652"/>
<pad name="31/A12" x="27.94" y="-7.62" drill="0.9652"/>
<pad name="32/A13" x="30.48" y="-7.62" drill="0.9652"/>
<pad name="33/A14" x="30.48" y="7.62" drill="0.9652"/>
<pad name="34/A15" x="27.94" y="7.62" drill="0.9652"/>
<pad name="35/A16" x="25.4" y="7.62" drill="0.9652"/>
<pad name="36/A17" x="22.86" y="7.62" drill="0.9652"/>
<pad name="37/A18" x="20.32" y="7.62" drill="0.9652"/>
<pad name="38/A19" x="17.78" y="7.62" drill="0.9652"/>
<pad name="39/A20" x="15.24" y="7.62" drill="0.9652"/>
<pad name="21/DAC0" x="12.7" y="7.62" drill="0.9652"/>
<pad name="A22/DAC1" x="10.16" y="7.62" drill="0.9652"/>
<pad name="VUSB" x="-25.4" y="5.08" drill="0.9652"/>
<pad name="AREF" x="-20.32" y="5.08" drill="0.9652"/>
<pad name="A10" x="-17.78" y="5.08" drill="0.9652"/>
<pad name="A11" x="-15.24" y="5.08" drill="0.9652"/>
<wire x1="-29.21" y1="8.89" x2="31.75" y2="8.89" width="0.3048" layer="21"/>
<wire x1="31.75" y1="-8.89" x2="-29.21" y2="-8.89" width="0.3048" layer="21"/>
<wire x1="8.636" y1="-6.35" x2="-3.556" y2="-6.35" width="0.3048" layer="21"/>
<wire x1="-3.556" y1="-6.35" x2="-3.81" y2="-6.096" width="0.3048" layer="21" curve="-90"/>
<wire x1="-3.81" y1="-6.096" x2="-3.81" y2="6.096" width="0.3048" layer="21"/>
<wire x1="-3.81" y1="6.096" x2="-3.556" y2="6.35" width="0.3048" layer="21" curve="-90"/>
<wire x1="-3.556" y1="6.35" x2="8.636" y2="6.35" width="0.3048" layer="21"/>
<wire x1="8.636" y1="6.35" x2="8.89" y2="6.096" width="0.3048" layer="21" curve="-90"/>
<wire x1="8.89" y1="6.096" x2="8.89" y2="-6.096" width="0.3048" layer="21"/>
<wire x1="8.89" y1="-6.096" x2="8.636" y2="-6.35" width="0.3048" layer="21" curve="-90"/>
<wire x1="31.75" y1="8.89" x2="31.75" y2="-8.89" width="0.3048" layer="21"/>
<wire x1="-25.4" y1="3.81" x2="-27.305" y2="3.81" width="0.3048" layer="21"/>
<wire x1="-27.305" y1="3.81" x2="-28.575" y2="3.81" width="0.3048" layer="21"/>
<wire x1="-28.575" y1="3.81" x2="-29.21" y2="3.81" width="0.3048" layer="21"/>
<wire x1="-29.21" y1="3.81" x2="-30.48" y2="3.81" width="0.3048" layer="21"/>
<wire x1="-30.48" y1="3.81" x2="-30.48" y2="-3.81" width="0.3048" layer="21"/>
<wire x1="-30.48" y1="-3.81" x2="-29.21" y2="-3.81" width="0.3048" layer="21"/>
<wire x1="-29.21" y1="-3.81" x2="-28.575" y2="-3.81" width="0.3048" layer="21"/>
<wire x1="-28.575" y1="-3.81" x2="-27.305" y2="-3.81" width="0.3048" layer="21"/>
<wire x1="-27.305" y1="-3.81" x2="-25.4" y2="-3.81" width="0.3048" layer="21"/>
<wire x1="-25.4" y1="-3.81" x2="-25.4" y2="-3.175" width="0.3048" layer="21"/>
<wire x1="-25.4" y1="-3.175" x2="-25.4" y2="-2.2225" width="0.3048" layer="21"/>
<wire x1="-25.4" y1="-2.2225" x2="-25.4" y2="-1.905" width="0.3048" layer="21"/>
<wire x1="-25.4" y1="-1.905" x2="-25.4" y2="-1.27" width="0.3048" layer="21"/>
<wire x1="-25.4" y1="-1.27" x2="-25.4" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-25.4" y1="-0.635" x2="-25.4" y2="0" width="0.3048" layer="21"/>
<wire x1="-25.4" y1="0" x2="-25.4" y2="0.635" width="0.3048" layer="21"/>
<wire x1="-25.4" y1="0.635" x2="-25.4" y2="1.27" width="0.3048" layer="21"/>
<wire x1="-25.4" y1="1.27" x2="-25.4" y2="1.905" width="0.3048" layer="21"/>
<wire x1="-25.4" y1="1.905" x2="-25.4" y2="2.2225" width="0.3048" layer="21"/>
<wire x1="-25.4" y1="2.2225" x2="-25.4" y2="3.175" width="0.3048" layer="21"/>
<wire x1="-25.4" y1="3.175" x2="-25.4" y2="3.81" width="0.3048" layer="21"/>
<wire x1="-29.21" y1="8.89" x2="-29.21" y2="3.81" width="0.3048" layer="21"/>
<wire x1="-29.21" y1="-8.89" x2="-29.21" y2="-3.81" width="0.3048" layer="21"/>
<wire x1="19.05" y1="5.08" x2="19.05" y2="6.096" width="0.127" layer="21"/>
<wire x1="19.05" y1="6.096" x2="19.304" y2="6.35" width="0.127" layer="21" curve="-90"/>
<wire x1="19.304" y1="6.35" x2="21.59" y2="6.35" width="0.127" layer="21"/>
<wire x1="21.59" y1="6.35" x2="23.495" y2="6.35" width="0.3048" layer="21"/>
<wire x1="23.495" y1="6.35" x2="25.4" y2="6.35" width="0.127" layer="21"/>
<wire x1="25.4" y1="6.35" x2="27.6225" y2="6.35" width="0.3048" layer="21"/>
<wire x1="27.6225" y1="6.35" x2="30.861" y2="6.35" width="0.3048" layer="21"/>
<wire x1="30.861" y1="6.35" x2="31.115" y2="6.096" width="0.3048" layer="21" curve="-90"/>
<wire x1="31.115" y1="6.096" x2="31.115" y2="5.3975" width="0.3048" layer="21"/>
<wire x1="31.115" y1="5.3975" x2="30.7975" y2="5.08" width="0.3048" layer="21" curve="-90"/>
<wire x1="30.7975" y1="5.08" x2="29.845" y2="5.08" width="0.3048" layer="21"/>
<wire x1="29.845" y1="5.08" x2="29.21" y2="4.445" width="0.3048" layer="21" curve="90"/>
<wire x1="29.21" y1="4.445" x2="29.21" y2="3.175" width="0.3048" layer="21"/>
<wire x1="29.21" y1="3.175" x2="29.21" y2="2.54" width="0.3048" layer="21"/>
<wire x1="29.21" y1="2.54" x2="29.21" y2="1.905" width="0.3048" layer="21"/>
<wire x1="29.21" y1="1.905" x2="29.21" y2="1.27" width="0.3048" layer="21"/>
<wire x1="29.21" y1="1.27" x2="29.21" y2="0.635" width="0.3048" layer="21"/>
<wire x1="29.21" y1="0.635" x2="29.21" y2="0" width="0.3048" layer="21"/>
<wire x1="29.21" y1="0" x2="29.21" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="29.21" y1="-0.635" x2="29.21" y2="-1.27" width="0.3048" layer="21"/>
<wire x1="29.21" y1="-1.27" x2="29.21" y2="-1.905" width="0.3048" layer="21"/>
<wire x1="29.21" y1="-1.905" x2="29.21" y2="-2.54" width="0.3048" layer="21"/>
<wire x1="29.21" y1="-2.54" x2="29.21" y2="-3.175" width="0.3048" layer="21"/>
<wire x1="29.21" y1="-3.175" x2="29.21" y2="-3.81" width="0.3048" layer="21"/>
<wire x1="29.21" y1="-3.81" x2="29.21" y2="-4.445" width="0.3048" layer="21"/>
<wire x1="29.21" y1="-4.445" x2="29.21" y2="-5.08" width="0.3048" layer="21"/>
<wire x1="29.21" y1="-5.08" x2="29.845" y2="-5.715" width="0.3048" layer="21" curve="90"/>
<wire x1="29.845" y1="-5.715" x2="30.7975" y2="-5.715" width="0.3048" layer="21"/>
<wire x1="30.7975" y1="-5.715" x2="31.115" y2="-6.0325" width="0.3048" layer="21" curve="-90"/>
<wire x1="31.115" y1="-6.0325" x2="31.115" y2="-6.096" width="0.3048" layer="21"/>
<wire x1="31.115" y1="-6.096" x2="30.861" y2="-6.35" width="0.3048" layer="21" curve="-90"/>
<wire x1="30.861" y1="-6.35" x2="19.304" y2="-6.35" width="0.3048" layer="21"/>
<wire x1="19.304" y1="-6.35" x2="19.05" y2="-6.096" width="0.3048" layer="21" curve="-90"/>
<wire x1="19.05" y1="-6.096" x2="19.05" y2="5.08" width="0.3048" layer="21"/>
<wire x1="29.21" y1="3.175" x2="30.861" y2="3.175" width="0.127" layer="21"/>
<wire x1="30.861" y1="3.175" x2="31.115" y2="2.921" width="0.127" layer="21" curve="-90"/>
<wire x1="31.115" y1="2.921" x2="31.115" y2="2.794" width="0.127" layer="21"/>
<wire x1="31.115" y1="2.794" x2="30.861" y2="2.54" width="0.127" layer="21" curve="-90"/>
<wire x1="30.861" y1="2.54" x2="29.21" y2="2.54" width="0.127" layer="21"/>
<wire x1="29.21" y1="1.905" x2="30.861" y2="1.905" width="0.127" layer="21"/>
<wire x1="30.861" y1="1.905" x2="31.115" y2="1.651" width="0.127" layer="21" curve="-90"/>
<wire x1="31.115" y1="1.651" x2="31.115" y2="1.524" width="0.127" layer="21"/>
<wire x1="31.115" y1="1.524" x2="30.861" y2="1.27" width="0.127" layer="21" curve="-90"/>
<wire x1="30.861" y1="1.27" x2="29.21" y2="1.27" width="0.127" layer="21"/>
<wire x1="29.21" y1="0.635" x2="30.861" y2="0.635" width="0.127" layer="21"/>
<wire x1="30.861" y1="0.635" x2="31.115" y2="0.381" width="0.127" layer="21" curve="-90"/>
<wire x1="31.115" y1="0.381" x2="31.115" y2="0.254" width="0.127" layer="21"/>
<wire x1="31.115" y1="0.254" x2="30.861" y2="0" width="0.127" layer="21" curve="-90"/>
<wire x1="30.861" y1="0" x2="29.21" y2="0" width="0.127" layer="21"/>
<wire x1="29.21" y1="-0.635" x2="30.861" y2="-0.635" width="0.127" layer="21"/>
<wire x1="30.861" y1="-0.635" x2="31.115" y2="-0.889" width="0.127" layer="21" curve="-90"/>
<wire x1="31.115" y1="-0.889" x2="31.115" y2="-1.016" width="0.127" layer="21"/>
<wire x1="31.115" y1="-1.016" x2="30.861" y2="-1.27" width="0.127" layer="21" curve="-90"/>
<wire x1="30.861" y1="-1.27" x2="29.21" y2="-1.27" width="0.127" layer="21"/>
<wire x1="29.21" y1="-1.905" x2="30.861" y2="-1.905" width="0.127" layer="21"/>
<wire x1="30.861" y1="-1.905" x2="31.115" y2="-2.159" width="0.127" layer="21" curve="-90"/>
<wire x1="31.115" y1="-2.159" x2="31.115" y2="-2.286" width="0.127" layer="21"/>
<wire x1="31.115" y1="-2.286" x2="30.861" y2="-2.54" width="0.127" layer="21" curve="-90"/>
<wire x1="30.861" y1="-2.54" x2="29.21" y2="-2.54" width="0.127" layer="21"/>
<wire x1="29.21" y1="-3.175" x2="30.861" y2="-3.175" width="0.127" layer="21"/>
<wire x1="30.861" y1="-3.175" x2="31.115" y2="-3.429" width="0.127" layer="21" curve="-90"/>
<wire x1="31.115" y1="-3.429" x2="31.115" y2="-3.556" width="0.127" layer="21"/>
<wire x1="31.115" y1="-3.556" x2="30.861" y2="-3.81" width="0.127" layer="21" curve="-90"/>
<wire x1="30.861" y1="-3.81" x2="29.21" y2="-3.81" width="0.127" layer="21"/>
<wire x1="29.21" y1="-4.445" x2="30.861" y2="-4.445" width="0.127" layer="21"/>
<wire x1="30.861" y1="-4.445" x2="31.115" y2="-4.699" width="0.127" layer="21" curve="-90"/>
<wire x1="31.115" y1="-4.699" x2="31.115" y2="-4.826" width="0.127" layer="21"/>
<wire x1="31.115" y1="-4.826" x2="30.861" y2="-5.08" width="0.127" layer="21" curve="-90"/>
<wire x1="30.861" y1="-5.08" x2="29.21" y2="-5.08" width="0.127" layer="21"/>
<wire x1="12.954" y1="-2.54" x2="16.256" y2="-2.54" width="0.127" layer="21"/>
<wire x1="16.256" y1="-2.54" x2="16.51" y2="-2.286" width="0.127" layer="21" curve="90"/>
<wire x1="16.51" y1="-2.286" x2="16.51" y2="2.286" width="0.127" layer="21"/>
<wire x1="16.51" y1="2.286" x2="16.256" y2="2.54" width="0.127" layer="21" curve="90"/>
<wire x1="16.256" y1="2.54" x2="12.954" y2="2.54" width="0.127" layer="21"/>
<wire x1="12.954" y1="2.54" x2="12.7" y2="2.286" width="0.127" layer="21" curve="90"/>
<wire x1="12.7" y1="2.286" x2="12.7" y2="-2.286" width="0.127" layer="21"/>
<wire x1="12.7" y1="-2.286" x2="12.954" y2="-2.54" width="0.127" layer="21" curve="90"/>
<wire x1="14.605" y1="-1.905" x2="13.335" y2="-0.635" width="0.3048" layer="21" curve="-90"/>
<wire x1="13.335" y1="-0.635" x2="13.335" y2="0.635" width="0.3048" layer="21"/>
<wire x1="13.335" y1="0.635" x2="14.605" y2="1.905" width="0.3048" layer="21" curve="-90"/>
<wire x1="14.605" y1="1.905" x2="15.875" y2="0.635" width="0.3048" layer="21" curve="-90"/>
<wire x1="15.875" y1="0.635" x2="15.875" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="15.875" y1="-0.635" x2="14.605" y2="-1.905" width="0.3048" layer="21" curve="-90"/>
<wire x1="-22.86" y1="-3.81" x2="-20.32" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-20.32" y1="-3.81" x2="-10.16" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-10.16" y1="-3.81" x2="-10.16" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-10.16" y1="-6.35" x2="-20.32" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-20.32" y1="-6.35" x2="-22.86" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-22.86" y1="-6.35" x2="-22.86" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-20.32" y1="-3.81" x2="-20.32" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-25.4" y1="0" x2="-24.13" y2="0" width="0.127" layer="21"/>
<wire x1="-25.4" y1="0.635" x2="-24.13" y2="0.635" width="0.127" layer="21"/>
<wire x1="-25.4" y1="1.27" x2="-24.13" y2="1.27" width="0.127" layer="21"/>
<wire x1="-25.4" y1="-0.635" x2="-24.13" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-25.4" y1="-1.27" x2="-24.13" y2="-1.27" width="0.127" layer="21"/>
<text x="2.69875" y="0" size="1.778" layer="21" ratio="12" distance="25" align="center">TEENSY
3.6</text>
<wire x1="19.05" y1="5.08" x2="21.336" y2="5.08" width="0.3048" layer="21"/>
<wire x1="21.336" y1="5.08" x2="21.59" y2="5.334" width="0.3048" layer="21" curve="90"/>
<wire x1="21.59" y1="5.334" x2="21.59" y2="6.35" width="0.3048" layer="21"/>
<wire x1="23.495" y1="6.35" x2="23.495" y2="5.334" width="0.3048" layer="21"/>
<wire x1="23.495" y1="5.334" x2="23.749" y2="5.08" width="0.3048" layer="21" curve="90"/>
<wire x1="23.749" y1="5.08" x2="25.146" y2="5.08" width="0.3048" layer="21"/>
<wire x1="25.146" y1="5.08" x2="25.4" y2="5.334" width="0.3048" layer="21" curve="90"/>
<wire x1="25.4" y1="5.334" x2="25.4" y2="6.35" width="0.3048" layer="21"/>
<wire x1="-25.4" y1="1.905" x2="-24.13" y2="1.905" width="0.127" layer="21"/>
<wire x1="-24.13" y1="1.905" x2="-24.13" y2="3.175" width="0.127" layer="21"/>
<wire x1="-24.13" y1="3.175" x2="-25.4" y2="3.175" width="0.127" layer="21"/>
<wire x1="-25.4" y1="-1.905" x2="-24.13" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-24.13" y1="-1.905" x2="-24.13" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-24.13" y1="-3.175" x2="-25.4" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-28.575" y1="3.81" x2="-28.575" y2="5.08" width="0.127" layer="21"/>
<wire x1="-28.575" y1="5.08" x2="-27.305" y2="5.08" width="0.127" layer="21"/>
<wire x1="-27.305" y1="5.08" x2="-27.305" y2="3.81" width="0.127" layer="21"/>
<wire x1="-28.575" y1="-3.81" x2="-28.575" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-28.575" y1="-5.08" x2="-27.305" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-27.305" y1="-5.08" x2="-27.305" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-29.845" y1="3.175" x2="-28.2575" y2="3.175" width="0.127" layer="21"/>
<wire x1="-28.2575" y1="3.175" x2="-27.94" y2="2.8575" width="0.127" layer="21" curve="-90"/>
<wire x1="-27.94" y1="2.8575" x2="-27.94" y2="2.2225" width="0.127" layer="21"/>
<wire x1="-27.94" y1="2.2225" x2="-28.2575" y2="1.905" width="0.127" layer="21" curve="-90"/>
<wire x1="-28.2575" y1="1.905" x2="-29.845" y2="1.905" width="0.127" layer="21"/>
<wire x1="-29.845" y1="-3.175" x2="-28.2575" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-28.2575" y1="-3.175" x2="-27.94" y2="-2.8575" width="0.127" layer="21" curve="90"/>
<wire x1="-27.94" y1="-2.8575" x2="-27.94" y2="-2.2225" width="0.127" layer="21"/>
<wire x1="-27.94" y1="-2.2225" x2="-28.2575" y2="-1.905" width="0.127" layer="21" curve="90"/>
<wire x1="-28.2575" y1="-1.905" x2="-29.845" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-29.845" y1="3.175" x2="-29.845" y2="1.905" width="0.127" layer="21"/>
<wire x1="-29.845" y1="-1.905" x2="-29.845" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-5.715" y1="-5.715" x2="-5.334" y2="-5.715" width="0.127" layer="21"/>
<wire x1="-5.334" y1="-5.715" x2="-5.08" y2="-5.461" width="0.127" layer="21" curve="90"/>
<wire x1="-5.08" y1="-5.461" x2="-5.08" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-5.08" x2="-5.08" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-4.445" x2="-5.08" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-3.81" x2="-5.08" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-3.175" x2="-5.08" y2="-2.794" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-2.794" x2="-5.334" y2="-2.54" width="0.127" layer="21" curve="90"/>
<wire x1="-5.334" y1="-2.54" x2="-5.715" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-5.715" y1="-2.54" x2="-6.35" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-2.54" x2="-6.985" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-6.985" y1="-2.54" x2="-7.62" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-7.62" y1="-2.54" x2="-8.001" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-8.001" y1="-2.54" x2="-8.255" y2="-2.794" width="0.127" layer="21" curve="90"/>
<wire x1="-8.255" y1="-2.794" x2="-8.255" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-8.255" y1="-3.175" x2="-8.255" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-8.255" y1="-3.81" x2="-8.255" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-8.255" y1="-4.445" x2="-8.255" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-8.255" y1="-5.08" x2="-8.255" y2="-5.461" width="0.127" layer="21"/>
<wire x1="-8.255" y1="-5.461" x2="-8.001" y2="-5.715" width="0.127" layer="21" curve="90"/>
<wire x1="-8.001" y1="-5.715" x2="-7.62" y2="-5.715" width="0.127" layer="21"/>
<wire x1="-7.62" y1="-5.715" x2="-6.985" y2="-5.715" width="0.127" layer="21"/>
<wire x1="-6.985" y1="-5.715" x2="-6.35" y2="-5.715" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-5.715" x2="-5.715" y2="-5.715" width="0.127" layer="21"/>
<wire x1="-13.335" y1="1.016" x2="-13.335" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-13.335" y1="-1.016" x2="-13.081" y2="-1.27" width="0.127" layer="21" curve="90"/>
<wire x1="-13.081" y1="-1.27" x2="-11.049" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-11.049" y1="-1.27" x2="-10.795" y2="-1.016" width="0.127" layer="21" curve="90"/>
<wire x1="-10.795" y1="-1.016" x2="-10.795" y2="1.016" width="0.127" layer="21"/>
<wire x1="-10.795" y1="1.016" x2="-11.049" y2="1.27" width="0.127" layer="21" curve="90"/>
<wire x1="-11.049" y1="1.27" x2="-13.081" y2="1.27" width="0.127" layer="21"/>
<wire x1="-13.081" y1="1.27" x2="-13.335" y2="1.016" width="0.127" layer="21" curve="90"/>
<wire x1="20.0025" y1="-2.159" x2="20.0025" y2="-2.6035" width="0.127" layer="21"/>
<wire x1="20.0025" y1="-2.6035" x2="20.2565" y2="-2.8575" width="0.127" layer="21" curve="90"/>
<wire x1="20.2565" y1="-2.8575" x2="21.336" y2="-2.8575" width="0.127" layer="21"/>
<wire x1="21.336" y1="-2.8575" x2="21.59" y2="-2.6035" width="0.127" layer="21" curve="90"/>
<wire x1="21.59" y1="-2.6035" x2="21.59" y2="-2.159" width="0.127" layer="21"/>
<wire x1="21.59" y1="-2.159" x2="21.336" y2="-1.905" width="0.127" layer="21" curve="90"/>
<wire x1="21.336" y1="-1.905" x2="20.2565" y2="-1.905" width="0.127" layer="21"/>
<wire x1="20.2565" y1="-1.905" x2="20.0025" y2="-2.159" width="0.127" layer="21" curve="90"/>
<wire x1="20.0025" y1="-3.81" x2="20.0025" y2="-5.3975" width="0.127" layer="21"/>
<wire x1="20.0025" y1="-5.3975" x2="21.59" y2="-5.3975" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-3.81" x2="-4.7625" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-4.445" x2="-4.7625" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-5.08" x2="-4.7625" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-3.175" x2="-4.7625" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-5.715" y1="-2.54" x2="-5.715" y2="-2.2225" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-2.54" x2="-6.35" y2="-2.2225" width="0.127" layer="21"/>
<wire x1="-6.985" y1="-2.54" x2="-6.985" y2="-2.2225" width="0.127" layer="21"/>
<wire x1="-7.62" y1="-2.54" x2="-7.62" y2="-2.2225" width="0.127" layer="21"/>
<wire x1="-8.255" y1="-3.175" x2="-8.5725" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-8.255" y1="-3.81" x2="-8.5725" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-8.255" y1="-4.445" x2="-8.5725" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-8.255" y1="-5.08" x2="-8.5725" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-5.715" y1="-5.715" x2="-5.715" y2="-6.0325" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-5.715" x2="-6.35" y2="-6.0325" width="0.127" layer="21"/>
<wire x1="-6.985" y1="-5.715" x2="-6.985" y2="-6.0325" width="0.127" layer="21"/>
<wire x1="-7.62" y1="-5.715" x2="-7.62" y2="-6.0325" width="0.127" layer="21"/>
<wire x1="27.6225" y1="6.35" x2="27.6225" y2="5.3975" width="0.127" layer="21"/>
<wire x1="27.6225" y1="5.3975" x2="27.94" y2="5.08" width="0.127" layer="21" curve="90"/>
<wire x1="27.94" y1="5.08" x2="28.8925" y2="5.08" width="0.127" layer="21"/>
<wire x1="28.8925" y1="5.08" x2="28.8925" y2="5.3975" width="0.127" layer="21"/>
<wire x1="28.8925" y1="5.3975" x2="29.21" y2="5.715" width="0.127" layer="21" curve="-90"/>
<wire x1="29.21" y1="5.715" x2="29.5275" y2="5.715" width="0.127" layer="21"/>
<wire x1="-25.4" y1="1.27" x2="-26.67" y2="1.27" width="0.127" layer="21"/>
<wire x1="-26.67" y1="1.27" x2="-26.67" y2="3.175" width="0.127" layer="21"/>
<wire x1="-26.67" y1="3.175" x2="-26.204921875" y2="3.175" width="0.127" layer="21"/>
<wire x1="-26.204921875" y1="3.175" x2="-25.94074375" y2="3.033615625" width="0.127" layer="21" curve="-56.310402"/>
<wire x1="-25.94074375" y1="3.033615625" x2="-25.4" y2="2.2225" width="0.127" layer="21"/>
<wire x1="-25.4" y1="-1.27" x2="-26.67" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-26.67" y1="-1.27" x2="-26.67" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-26.67" y1="-3.175" x2="-26.204921875" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-26.204921875" y1="-3.175" x2="-25.94074375" y2="-3.033615625" width="0.127" layer="21" curve="56.310402"/>
<wire x1="-25.94074375" y1="-3.033615625" x2="-25.4" y2="-2.2225" width="0.127" layer="21"/>
<circle x="-21.59" y="-5.08" radius="0.635" width="0.3048" layer="21"/>
<circle x="-19.05" y="-5.08" radius="0.635" width="0.3048" layer="21"/>
<circle x="-16.51" y="-5.08" radius="0.635" width="0.3048" layer="21"/>
<circle x="-13.97" y="-5.08" radius="0.635" width="0.3048" layer="21"/>
<circle x="-11.43" y="-5.08" radius="0.635" width="0.3048" layer="21"/>
<circle x="17.78" y="-5.08" radius="0.635" width="0.3048" layer="21"/>
<circle x="17.78" y="-2.54" radius="0.635" width="0.3048" layer="21"/>
<circle x="17.78" y="0" radius="0.635" width="0.3048" layer="21"/>
<circle x="17.78" y="2.54" radius="0.635" width="0.3048" layer="21"/>
<circle x="17.78" y="5.08" radius="0.635" width="0.3048" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="TEENSY_3.5/3.6_BASIC">
<wire x1="-15.24" y1="27.94" x2="15.24" y2="27.94" width="0.254" layer="94"/>
<wire x1="15.24" y1="27.94" x2="15.24" y2="-48.26" width="0.254" layer="94"/>
<wire x1="-15.24" y1="27.94" x2="-15.24" y2="-48.26" width="0.254" layer="94"/>
<pin name="1/TX" x="-20.32" y="7.62" visible="pin" length="middle"/>
<pin name="2" x="-20.32" y="5.08" visible="pin" length="middle"/>
<pin name="3/CANTX" x="-20.32" y="2.54" visible="pin" length="middle"/>
<pin name="4/CANRX" x="-20.32" y="0" visible="pin" length="middle"/>
<pin name="0/RX" x="-20.32" y="10.16" visible="pin" length="middle"/>
<pin name="5" x="-20.32" y="-2.54" visible="pin" length="middle"/>
<pin name="6" x="-20.32" y="-5.08" visible="pin" length="middle"/>
<pin name="7" x="-20.32" y="-7.62" visible="pin" length="middle"/>
<pin name="8" x="-20.32" y="-10.16" visible="pin" length="middle"/>
<pin name="9/CS" x="-20.32" y="-12.7" visible="pin" length="middle"/>
<pin name="10/CS" x="-20.32" y="-15.24" visible="pin" length="middle"/>
<pin name="11/MOSI" x="-20.32" y="-17.78" visible="pin" length="middle"/>
<pin name="12/MISO" x="-20.32" y="-20.32" visible="pin" length="middle"/>
<pin name="3V3" x="-20.32" y="-22.86" visible="pin" length="middle"/>
<pin name="24" x="-20.32" y="-25.4" visible="pin" length="middle"/>
<pin name="25" x="-20.32" y="-27.94" visible="pin" length="middle"/>
<pin name="26" x="-20.32" y="-30.48" visible="pin" length="middle"/>
<pin name="27" x="-20.32" y="-33.02" visible="pin" length="middle"/>
<pin name="28" x="-20.32" y="-35.56" visible="pin" length="middle"/>
<pin name="29" x="-20.32" y="-38.1" visible="pin" length="middle"/>
<pin name="30" x="-20.32" y="-40.64" visible="pin" length="middle"/>
<pin name="31/A12" x="-20.32" y="-43.18" visible="pin" length="middle"/>
<pin name="32/A13" x="-20.32" y="-45.72" visible="pin" length="middle"/>
<pin name="GND" x="-20.32" y="12.7" visible="pin" length="middle"/>
<wire x1="-15.24" y1="-48.26" x2="15.24" y2="-48.26" width="0.254" layer="94"/>
<pin name="VIN" x="20.32" y="12.7" visible="pin" length="middle" rot="R180"/>
<pin name="AGND" x="20.32" y="10.16" visible="pin" length="middle" rot="R180"/>
<pin name="3V3_LO" x="20.32" y="7.62" visible="pin" length="middle" rot="R180"/>
<pin name="23/A9" x="20.32" y="5.08" visible="pin" length="middle" rot="R180"/>
<pin name="22/A8" x="20.32" y="2.54" visible="pin" length="middle" rot="R180"/>
<pin name="21/A7" x="20.32" y="0" visible="pin" length="middle" rot="R180"/>
<pin name="20/A6" x="20.32" y="-2.54" visible="pin" length="middle" rot="R180"/>
<pin name="19/A5/SCL" x="20.32" y="-5.08" visible="pin" length="middle" rot="R180"/>
<pin name="18/A4/SDA" x="20.32" y="-7.62" visible="pin" length="middle" rot="R180"/>
<pin name="17/A3" x="20.32" y="-10.16" visible="pin" length="middle" rot="R180"/>
<pin name="16/A2" x="20.32" y="-12.7" visible="pin" length="middle" rot="R180"/>
<pin name="15/A1" x="20.32" y="-15.24" visible="pin" length="middle" rot="R180"/>
<pin name="14/A0" x="20.32" y="-17.78" visible="pin" length="middle" rot="R180"/>
<pin name="13/SCK" x="20.32" y="-20.32" visible="pin" length="middle" rot="R180"/>
<pin name="GND2" x="20.32" y="-22.86" visible="pin" length="middle" rot="R180"/>
<pin name="A22/DAC1" x="20.32" y="-25.4" visible="pin" length="middle" rot="R180"/>
<pin name="A21/DAC0" x="20.32" y="-27.94" visible="pin" length="middle" rot="R180"/>
<pin name="39/A20" x="20.32" y="-30.48" visible="pin" length="middle" rot="R180"/>
<pin name="38/A19" x="20.32" y="-33.02" visible="pin" length="middle" rot="R180"/>
<pin name="37/A18" x="20.32" y="-35.56" visible="pin" length="middle" rot="R180"/>
<pin name="36/A17" x="20.32" y="-38.1" visible="pin" length="middle" rot="R180"/>
<pin name="35/A16" x="20.32" y="-40.64" visible="pin" length="middle" rot="R180"/>
<pin name="34/A15" x="20.32" y="-43.18" visible="pin" length="middle" rot="R180"/>
<pin name="33/A14" x="20.32" y="-45.72" visible="pin" length="middle" rot="R180"/>
<pin name="VUSB" x="20.32" y="25.4" visible="pin" length="middle" rot="R180"/>
<pin name="AREF" x="20.32" y="22.86" visible="pin" length="middle" rot="R180"/>
<pin name="A10" x="20.32" y="20.32" visible="pin" length="middle" rot="R180"/>
<pin name="A11" x="20.32" y="17.78" visible="pin" length="middle" rot="R180"/>
<text x="-14.478" y="25.146" size="1.778" layer="94">Teensy 3.6</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="TEENSY_3.5/3.6_BASIC">
<gates>
<gate name="G$1" symbol="TEENSY_3.5/3.6_BASIC" x="0" y="10.16"/>
</gates>
<devices>
<device name="" package="TEENSY_3.5/3.6_BASIC">
<connects>
<connect gate="G$1" pin="0/RX" pad="0/RX"/>
<connect gate="G$1" pin="1/TX" pad="1/TX"/>
<connect gate="G$1" pin="10/CS" pad="10/CS"/>
<connect gate="G$1" pin="11/MOSI" pad="11/MOSI"/>
<connect gate="G$1" pin="12/MISO" pad="12/MISO"/>
<connect gate="G$1" pin="13/SCK" pad="13/SCK"/>
<connect gate="G$1" pin="14/A0" pad="14/A0"/>
<connect gate="G$1" pin="15/A1" pad="15/A1"/>
<connect gate="G$1" pin="16/A2" pad="16/A2"/>
<connect gate="G$1" pin="17/A3" pad="17/A3"/>
<connect gate="G$1" pin="18/A4/SDA" pad="18/A4/SDA"/>
<connect gate="G$1" pin="19/A5/SCL" pad="19/A5/SCL"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="20/A6" pad="20/A6"/>
<connect gate="G$1" pin="21/A7" pad="21/A7"/>
<connect gate="G$1" pin="22/A8" pad="22/A8"/>
<connect gate="G$1" pin="23/A9" pad="23/A9"/>
<connect gate="G$1" pin="24" pad="24"/>
<connect gate="G$1" pin="25" pad="25"/>
<connect gate="G$1" pin="26" pad="26"/>
<connect gate="G$1" pin="27" pad="27"/>
<connect gate="G$1" pin="28" pad="28"/>
<connect gate="G$1" pin="29" pad="29"/>
<connect gate="G$1" pin="3/CANTX" pad="3/CANTX"/>
<connect gate="G$1" pin="30" pad="30"/>
<connect gate="G$1" pin="31/A12" pad="31/A12"/>
<connect gate="G$1" pin="32/A13" pad="32/A13"/>
<connect gate="G$1" pin="33/A14" pad="33/A14"/>
<connect gate="G$1" pin="34/A15" pad="34/A15"/>
<connect gate="G$1" pin="35/A16" pad="35/A16"/>
<connect gate="G$1" pin="36/A17" pad="36/A17"/>
<connect gate="G$1" pin="37/A18" pad="37/A18"/>
<connect gate="G$1" pin="38/A19" pad="38/A19"/>
<connect gate="G$1" pin="39/A20" pad="39/A20"/>
<connect gate="G$1" pin="3V3" pad="3V3"/>
<connect gate="G$1" pin="3V3_LO" pad="3V3_LO"/>
<connect gate="G$1" pin="4/CANRX" pad="4/CANRX"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9/CS" pad="9/CS"/>
<connect gate="G$1" pin="A10" pad="A10"/>
<connect gate="G$1" pin="A11" pad="A11"/>
<connect gate="G$1" pin="A21/DAC0" pad="21/DAC0"/>
<connect gate="G$1" pin="A22/DAC1" pad="A22/DAC1"/>
<connect gate="G$1" pin="AGND" pad="AGND"/>
<connect gate="G$1" pin="AREF" pad="AREF"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="GND2" pad="GND2"/>
<connect gate="G$1" pin="VIN" pad="VIN"/>
<connect gate="G$1" pin="VUSB" pad="VUSB"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="_kantaParts">
<packages>
<package name="RJ45-MOLEX95001">
<wire x1="-7.527" y1="8.279" x2="7.527" y2="8.279" width="0.2032" layer="21"/>
<wire x1="7.527" y1="-10.322" x2="-7.527" y2="-10.322" width="0.2032" layer="21"/>
<wire x1="-7.527" y1="-10.322" x2="-7.527" y2="8.279" width="0.2032" layer="21"/>
<wire x1="7.527" y1="8.279" x2="7.527" y2="-10.322" width="0.2032" layer="21"/>
<pad name="4" x="-0.635" y="6.35" drill="0.9" diameter="1.4"/>
<pad name="3" x="-1.905" y="3.81" drill="0.9" diameter="1.4"/>
<pad name="2" x="-3.175" y="6.35" drill="0.9" diameter="1.4"/>
<pad name="5" x="0.635" y="3.81" drill="0.9" diameter="1.4"/>
<pad name="1" x="-4.445" y="3.81" drill="0.9" diameter="1.4"/>
<pad name="6" x="1.905" y="6.35" drill="0.9" diameter="1.4"/>
<pad name="7" x="3.175" y="3.81" drill="0.9" diameter="1.4"/>
<pad name="8" x="4.445" y="6.35" drill="0.9" diameter="1.4"/>
<text x="-9.525" y="-3.175" size="1.778" layer="25" rot="R90">&gt;NAME</text>
<text x="-5.715" y="0" size="1.778" layer="27">&gt;VALUE</text>
<rectangle x1="7.6" y1="-8.025" x2="8.875" y2="-6.882" layer="21"/>
<rectangle x1="-8.875" y1="-8.025" x2="-7.625" y2="-6.882" layer="21"/>
<hole x="-5.715" y="-2.54" drill="3.2512"/>
<hole x="5.715" y="-2.54" drill="3.2512"/>
</package>
<package name="RJ45-VERTICAL">
<pad name="4" x="-0.135" y="6.35" drill="0.9" diameter="1.4"/>
<pad name="3" x="-1.405" y="3.81" drill="0.9" diameter="1.4"/>
<pad name="2" x="-2.675" y="6.35" drill="0.9" diameter="1.4"/>
<pad name="5" x="1.135" y="3.81" drill="0.9" diameter="1.4"/>
<pad name="1" x="-3.945" y="3.81" drill="0.9" diameter="1.4"/>
<pad name="6" x="2.405" y="6.35" drill="0.9" diameter="1.4"/>
<pad name="7" x="3.675" y="3.81" drill="0.9" diameter="1.4"/>
<pad name="8" x="4.945" y="6.35" drill="0.9" diameter="1.4"/>
<text x="-9.025" y="-3.175" size="1.778" layer="25" font="vector" rot="R90">&gt;NAME</text>
<text x="-5.215" y="0" size="1.778" layer="27" font="vector">&gt;VALUE</text>
<hole x="-5.215" y="-2.54" drill="3.2512"/>
<hole x="6.215" y="-2.54" drill="3.2512"/>
<wire x1="8" y1="7" x2="8" y2="-6" width="0.127" layer="21"/>
<wire x1="8" y1="-6" x2="-7" y2="-6" width="0.127" layer="21"/>
<wire x1="-7" y1="-6" x2="-7" y2="7" width="0.127" layer="21"/>
<wire x1="-7" y1="7" x2="8" y2="7" width="0.127" layer="21"/>
<rectangle x1="8" y1="-3" x2="9" y2="4" layer="21"/>
<rectangle x1="-8" y1="-3" x2="-7" y2="4" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="JACK8">
<wire x1="1.524" y1="8.128" x2="0" y2="8.128" width="0.254" layer="94"/>
<wire x1="0" y1="8.128" x2="0" y2="7.112" width="0.254" layer="94"/>
<wire x1="0" y1="7.112" x2="1.524" y2="7.112" width="0.254" layer="94"/>
<wire x1="1.524" y1="5.588" x2="0" y2="5.588" width="0.254" layer="94"/>
<wire x1="0" y1="5.588" x2="0" y2="4.572" width="0.254" layer="94"/>
<wire x1="0" y1="4.572" x2="1.524" y2="4.572" width="0.254" layer="94"/>
<wire x1="1.524" y1="3.048" x2="0" y2="3.048" width="0.254" layer="94"/>
<wire x1="0" y1="3.048" x2="0" y2="2.032" width="0.254" layer="94"/>
<wire x1="0" y1="2.032" x2="1.524" y2="2.032" width="0.254" layer="94"/>
<wire x1="1.524" y1="0.508" x2="0" y2="0.508" width="0.254" layer="94"/>
<wire x1="0" y1="0.508" x2="0" y2="-0.508" width="0.254" layer="94"/>
<wire x1="0" y1="-0.508" x2="1.524" y2="-0.508" width="0.254" layer="94"/>
<wire x1="1.524" y1="-2.032" x2="0" y2="-2.032" width="0.254" layer="94"/>
<wire x1="0" y1="-2.032" x2="0" y2="-3.048" width="0.254" layer="94"/>
<wire x1="0" y1="-3.048" x2="1.524" y2="-3.048" width="0.254" layer="94"/>
<wire x1="1.524" y1="-4.572" x2="0" y2="-4.572" width="0.254" layer="94"/>
<wire x1="0" y1="-4.572" x2="0" y2="-5.588" width="0.254" layer="94"/>
<wire x1="0" y1="-5.588" x2="1.524" y2="-5.588" width="0.254" layer="94"/>
<wire x1="3.048" y1="-2.032" x2="5.588" y2="-2.032" width="0.1998" layer="94"/>
<wire x1="5.588" y1="-2.032" x2="5.588" y2="-0.254" width="0.1998" layer="94"/>
<wire x1="5.588" y1="-0.254" x2="6.604" y2="-0.254" width="0.1998" layer="94"/>
<wire x1="6.604" y1="-0.254" x2="6.604" y2="1.778" width="0.1998" layer="94"/>
<wire x1="6.604" y1="1.778" x2="5.588" y2="1.778" width="0.1998" layer="94"/>
<wire x1="5.588" y1="1.778" x2="5.588" y2="3.556" width="0.1998" layer="94"/>
<wire x1="5.588" y1="3.556" x2="3.048" y2="3.556" width="0.1998" layer="94"/>
<wire x1="3.048" y1="3.556" x2="3.048" y2="2.54" width="0.1998" layer="94"/>
<wire x1="3.048" y1="2.54" x2="3.048" y2="2.032" width="0.1998" layer="94"/>
<wire x1="3.048" y1="2.032" x2="3.048" y2="1.524" width="0.1998" layer="94"/>
<wire x1="3.048" y1="1.524" x2="3.048" y2="1.016" width="0.1998" layer="94"/>
<wire x1="3.048" y1="1.016" x2="3.048" y2="0.508" width="0.1998" layer="94"/>
<wire x1="3.048" y1="0.508" x2="3.048" y2="0" width="0.1998" layer="94"/>
<wire x1="3.048" y1="0" x2="3.048" y2="-0.508" width="0.1998" layer="94"/>
<wire x1="3.048" y1="-0.508" x2="3.048" y2="-1.016" width="0.1998" layer="94"/>
<wire x1="3.048" y1="-1.016" x2="3.048" y2="-2.032" width="0.1998" layer="94"/>
<wire x1="3.048" y1="2.54" x2="3.81" y2="2.54" width="0.1998" layer="94"/>
<wire x1="3.048" y1="2.032" x2="3.81" y2="2.032" width="0.1998" layer="94"/>
<wire x1="3.048" y1="1.524" x2="3.81" y2="1.524" width="0.1998" layer="94"/>
<wire x1="3.048" y1="1.016" x2="3.81" y2="1.016" width="0.1998" layer="94"/>
<wire x1="3.048" y1="0.508" x2="3.81" y2="0.508" width="0.1998" layer="94"/>
<wire x1="3.048" y1="0" x2="3.81" y2="0" width="0.1998" layer="94"/>
<wire x1="1.524" y1="-7.112" x2="0" y2="-7.112" width="0.254" layer="94"/>
<wire x1="0" y1="-7.112" x2="0" y2="-8.128" width="0.254" layer="94"/>
<wire x1="0" y1="-8.128" x2="1.524" y2="-8.128" width="0.254" layer="94"/>
<wire x1="1.524" y1="-9.652" x2="0" y2="-9.652" width="0.254" layer="94"/>
<wire x1="0" y1="-9.652" x2="0" y2="-10.668" width="0.254" layer="94"/>
<wire x1="0" y1="-10.668" x2="1.524" y2="-10.668" width="0.254" layer="94"/>
<wire x1="3.048" y1="-0.508" x2="3.81" y2="-0.508" width="0.1998" layer="94"/>
<wire x1="3.048" y1="-1.016" x2="3.81" y2="-1.016" width="0.1998" layer="94"/>
<text x="-2.54" y="10.16" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-13.208" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="7.62" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="3" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="4" x="-2.54" y="0" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="5" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="6" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="7" x="-2.54" y="-7.62" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="8" x="-2.54" y="-10.16" visible="pad" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RJ45" prefix="X">
<description>&lt;b&gt;CORCOM Modular RJ Jacks&lt;/b&gt; No Shield&lt;p&gt;
Source: www.tycoelectronics.com .. ENG_DS_1654001_1099_RJ_L_0507.pdf&lt;/p&gt;&lt;br /&gt;
&lt;br/ &gt;
&lt;b&gt;TE_V&lt;/B&gt;&lt;br&gt;
TE Connectivity Vertical RJ45
&lt;a href="http://www.te.com/catalog/pn/en/216550-1"&gt;216550-1&lt;/a&gt;&lt;br /&gt;
RS &lt;a href="http://jp.rs-online.com/web/p/rj45-connectors/6154298/"&gt;615-4298&lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="JACK8" x="0" y="0"/>
</gates>
<devices>
<device name="MOLEX" package="RJ45-MOLEX95001">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TE_V" package="RJ45-VERTICAL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-LED">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find discrete LEDs for illumination or indication, but no displays.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="LED-0603">
<wire x1="0.46" y1="0.17" x2="0" y2="0.17" width="0.2032" layer="21"/>
<wire x1="-0.46" y1="0.17" x2="0" y2="0.17" width="0.2032" layer="21"/>
<wire x1="0" y1="0.17" x2="0.2338" y2="-0.14" width="0.2032" layer="21"/>
<wire x1="-0.0254" y1="0.1546" x2="-0.2184" y2="-0.14" width="0.2032" layer="21"/>
<smd name="C" x="0" y="0.877" dx="1" dy="1" layer="1" roundness="30"/>
<smd name="A" x="0" y="-0.877" dx="1" dy="1" layer="1" roundness="30"/>
<text x="-0.6985" y="-0.889" size="0.4064" layer="25" rot="R90">&gt;NAME</text>
<text x="1.0795" y="-1.016" size="0.4064" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="LED-1206">
<wire x1="-1" y1="1" x2="-2.4" y2="1" width="0.2032" layer="21"/>
<wire x1="-2.4" y1="1" x2="-2.4" y2="-1" width="0.2032" layer="21"/>
<wire x1="-2.4" y1="-1" x2="-1" y2="-1" width="0.2032" layer="21"/>
<wire x1="1" y1="1" x2="2.4" y2="1" width="0.2032" layer="21"/>
<wire x1="2.4" y1="1" x2="2.4" y2="-1" width="0.2032" layer="21"/>
<wire x1="2.4" y1="-1" x2="1" y2="-1" width="0.2032" layer="21"/>
<wire x1="0.3" y1="0.7" x2="0.3" y2="0" width="0.2032" layer="21"/>
<wire x1="0.3" y1="0" x2="0.3" y2="-0.7" width="0.2032" layer="21"/>
<wire x1="0.3" y1="0" x2="-0.3" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.3" y1="0.6" x2="-0.3" y2="-0.6" width="0.2032" layer="21"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="0" width="0.2032" layer="21"/>
<smd name="A" x="-1.5" y="0" dx="1.2" dy="1.4" layer="1"/>
<smd name="C" x="1.5" y="0" dx="1.2" dy="1.4" layer="1"/>
<text x="-0.889" y="1.397" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.778" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="LED-1206-BOTTOM">
<wire x1="-2" y1="0.4" x2="-2" y2="-0.4" width="0.127" layer="49"/>
<wire x1="-2.4" y1="0" x2="-1.6" y2="0" width="0.127" layer="49"/>
<wire x1="1.6" y1="0" x2="2.4" y2="0" width="0.127" layer="49"/>
<wire x1="-1.27" y1="0" x2="-0.381" y2="0" width="0.127" layer="49"/>
<wire x1="-0.381" y1="0" x2="-0.381" y2="0.635" width="0.127" layer="49"/>
<wire x1="-0.381" y1="0.635" x2="0.254" y2="0" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="-0.381" y2="-0.635" width="0.127" layer="49"/>
<wire x1="-0.381" y1="-0.635" x2="-0.381" y2="0" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.635" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.635" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="1.27" y2="0" width="0.127" layer="49"/>
<rectangle x1="-0.75" y1="-0.75" x2="0.75" y2="0.75" layer="51"/>
<smd name="A" x="-1.8" y="0" dx="1.5" dy="1.6" layer="1"/>
<smd name="C" x="1.8" y="0" dx="1.5" dy="1.6" layer="1"/>
<hole x="0" y="0" drill="2.3"/>
<polygon width="0" layer="51">
<vertex x="1.1" y="-0.5"/>
<vertex x="1.1" y="0.5"/>
<vertex x="1.6" y="0.5"/>
<vertex x="1.6" y="0.25" curve="90"/>
<vertex x="1.4" y="0.05"/>
<vertex x="1.4" y="-0.05" curve="90"/>
<vertex x="1.6" y="-0.25"/>
<vertex x="1.6" y="-0.5"/>
</polygon>
<polygon width="0" layer="51">
<vertex x="-1.1" y="0.5"/>
<vertex x="-1.1" y="-0.5"/>
<vertex x="-1.6" y="-0.5"/>
<vertex x="-1.6" y="-0.25" curve="90"/>
<vertex x="-1.4" y="-0.05"/>
<vertex x="-1.4" y="0.05" curve="90"/>
<vertex x="-1.6" y="0.25"/>
<vertex x="-1.6" y="0.5"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="LED">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="3.556" y="-4.572" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-4.572" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED-GREEN" prefix="D" uservalue="yes">
<description>&lt;B&gt;Various green LEDs&lt;br&gt;&lt;br&gt;&lt;/B&gt;
Green LEDs used in SFE Production&lt;br&gt;&lt;br&gt;
0603- DIO-00821&lt;br&gt;
LARGE- DIO-00862&lt;br&gt;
LILYPAD- DIO-09910&lt;br&gt;</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="0603" package="LED-0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-00821" constant="no"/>
<attribute name="VALUE" value="GREEN" constant="no"/>
</technology>
</technologies>
</device>
<device name="LILYPAD" package="LED-1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-09910"/>
<attribute name="VALUE" value="GREEN" constant="no"/>
</technology>
</technologies>
</device>
<device name="LARGE" package="LED-1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-00862" constant="no"/>
<attribute name="VALUE" value="GREEN" constant="no"/>
</technology>
</technologies>
</device>
<device name="1206-BOTTOM" package="LED-1206-BOTTOM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-11076" constant="no"/>
<attribute name="VALUE" value="Green" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="switch-copal">
<description>&lt;b&gt; SMT slide switches &lt;/b&gt;&lt;p&gt;
COPAL ELECTRONICS SMT SLIDE SWITCHES&lt;p&gt;
&lt;b&gt;--- CHS Series ---&lt;/b&gt;&lt;p&gt;
Features:
&lt;ul&gt;
&lt;li&gt;Meets EIAJ SOP standard outline dimensions (B type)
&lt;li&gt;Low profile design of 2.5mm
&lt;li&gt;12 types of 1, 2, 4, 6, 8 or 10 bit with either gull wing or J-hook available
&lt;li&gt;Excellent contact stability by twin Au-plated contact
&lt;/ul&gt;
&lt;author&gt;Created by richard@sacher-laser.com&lt;/author&gt;&lt;p&gt;
&lt;p&gt;
for further informations see also :&lt;p&gt;
http://www.copal-electronics.com</description>
<packages>
<package name="CHS-06A">
<description>6 bits, J-hook</description>
<wire x1="-1.3918" y1="-2.688" x2="-0.635" y2="-2.688" width="0.127" layer="21"/>
<wire x1="-0.635" y1="-2.688" x2="6.985" y2="-2.688" width="0.127" layer="51"/>
<wire x1="6.985" y1="-2.688" x2="7.7482" y2="-2.688" width="0.127" layer="21"/>
<wire x1="7.7482" y1="-2.688" x2="7.7482" y2="2.712" width="0.127" layer="21"/>
<wire x1="7.7482" y1="2.712" x2="6.985" y2="2.712" width="0.127" layer="21"/>
<wire x1="6.985" y1="2.712" x2="-0.635" y2="2.712" width="0.127" layer="51"/>
<wire x1="-0.635" y1="2.712" x2="-1.3918" y2="2.712" width="0.127" layer="21"/>
<wire x1="-1.3918" y1="2.712" x2="-1.3918" y2="-2.688" width="0.127" layer="21"/>
<wire x1="-0.3175" y1="0.9525" x2="0.3175" y2="0.9525" width="0.127" layer="21"/>
<wire x1="0.3175" y1="0.9525" x2="0.3175" y2="-0.9525" width="0.127" layer="21"/>
<wire x1="0.3175" y1="-0.9525" x2="-0.3175" y2="-0.9525" width="0.127" layer="21"/>
<wire x1="-0.3175" y1="-0.9525" x2="-0.3175" y2="0.9525" width="0.127" layer="21"/>
<wire x1="0.9525" y1="0.9525" x2="1.5875" y2="0.9525" width="0.127" layer="21"/>
<wire x1="1.5875" y1="0.9525" x2="1.5875" y2="-0.9525" width="0.127" layer="21"/>
<wire x1="1.5875" y1="-0.9525" x2="0.9525" y2="-0.9525" width="0.127" layer="21"/>
<wire x1="0.9525" y1="-0.9525" x2="0.9525" y2="0.9525" width="0.127" layer="21"/>
<wire x1="2.2225" y1="0.9525" x2="2.8575" y2="0.9525" width="0.127" layer="21"/>
<wire x1="2.8575" y1="0.9525" x2="2.8575" y2="-0.9525" width="0.127" layer="21"/>
<wire x1="2.8575" y1="-0.9525" x2="2.2225" y2="-0.9525" width="0.127" layer="21"/>
<wire x1="2.2225" y1="-0.9525" x2="2.2225" y2="0.9525" width="0.127" layer="21"/>
<wire x1="3.4925" y1="0.9525" x2="4.1275" y2="0.9525" width="0.127" layer="21"/>
<wire x1="4.1275" y1="0.9525" x2="4.1275" y2="-0.9525" width="0.127" layer="21"/>
<wire x1="4.1275" y1="-0.9525" x2="3.4925" y2="-0.9525" width="0.127" layer="21"/>
<wire x1="3.4925" y1="-0.9525" x2="3.4925" y2="0.9525" width="0.127" layer="21"/>
<wire x1="4.7625" y1="0.9525" x2="5.3975" y2="0.9525" width="0.127" layer="21"/>
<wire x1="5.3975" y1="0.9525" x2="5.3975" y2="-0.9525" width="0.127" layer="21"/>
<wire x1="5.3975" y1="-0.9525" x2="4.7625" y2="-0.9525" width="0.127" layer="21"/>
<wire x1="4.7625" y1="-0.9525" x2="4.7625" y2="0.9525" width="0.127" layer="21"/>
<wire x1="6.0325" y1="0.9525" x2="6.6675" y2="0.9525" width="0.127" layer="21"/>
<wire x1="6.6675" y1="0.9525" x2="6.6675" y2="-0.9525" width="0.127" layer="21"/>
<wire x1="6.6675" y1="-0.9525" x2="6.0325" y2="-0.9525" width="0.127" layer="21"/>
<wire x1="6.0325" y1="-0.9525" x2="6.0325" y2="0.9525" width="0.127" layer="21"/>
<smd name="1" x="0" y="-2.54" dx="0.76" dy="1.6" layer="1"/>
<smd name="2" x="1.27" y="-2.54" dx="0.76" dy="1.6" layer="1"/>
<smd name="4" x="3.81" y="-2.54" dx="0.76" dy="1.6" layer="1"/>
<smd name="3" x="2.54" y="-2.54" dx="0.76" dy="1.6" layer="1"/>
<smd name="5" x="5.08" y="-2.54" dx="0.76" dy="1.6" layer="1"/>
<smd name="6" x="6.35" y="-2.54" dx="0.76" dy="1.6" layer="1"/>
<smd name="7" x="6.35" y="2.54" dx="0.76" dy="1.6" layer="1"/>
<smd name="8" x="5.08" y="2.54" dx="0.76" dy="1.6" layer="1"/>
<smd name="9" x="3.81" y="2.54" dx="0.76" dy="1.6" layer="1"/>
<smd name="10" x="2.54" y="2.54" dx="0.76" dy="1.6" layer="1"/>
<smd name="11" x="1.27" y="2.54" dx="0.76" dy="1.6" layer="1"/>
<smd name="12" x="0" y="2.54" dx="0.76" dy="1.6" layer="1"/>
<text x="-1.905" y="-2.54" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="9.525" y="-2.54" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="0.3175" y="1.5875" size="0.8128" layer="51" rot="R90">O</text>
<text x="1.5875" y="1.5875" size="0.8128" layer="51" rot="R90">N</text>
<text x="0.3175" y="-1.905" size="0.8128" layer="51" rot="R90">1</text>
<text x="1.5875" y="-1.905" size="0.8128" layer="51" rot="R90">2</text>
<text x="2.8575" y="-1.905" size="0.8128" layer="51" rot="R90">3</text>
<text x="4.1275" y="-1.905" size="0.8128" layer="51" rot="R90">4</text>
<text x="5.3975" y="-1.905" size="0.8128" layer="51" rot="R90">5</text>
<text x="6.6675" y="-1.905" size="0.8128" layer="51" rot="R90">6</text>
<rectangle x1="-0.3175" y1="-0.635" x2="0.3175" y2="0" layer="21"/>
<rectangle x1="0.9525" y1="-0.635" x2="1.5875" y2="0" layer="21"/>
<rectangle x1="2.2225" y1="-0.635" x2="2.8575" y2="0" layer="21"/>
<rectangle x1="3.4925" y1="-0.635" x2="4.1275" y2="0" layer="21"/>
<rectangle x1="4.7625" y1="-0.635" x2="5.3975" y2="0" layer="21"/>
<rectangle x1="6.0325" y1="-0.635" x2="6.6675" y2="0" layer="21"/>
</package>
<package name="CHS-06B">
<description>6 bits, gull wing</description>
<wire x1="-1.3918" y1="-2.688" x2="7.7482" y2="-2.688" width="0.127" layer="21"/>
<wire x1="7.7482" y1="-2.688" x2="7.7482" y2="2.712" width="0.127" layer="21"/>
<wire x1="7.7482" y1="2.712" x2="-1.3918" y2="2.712" width="0.127" layer="21"/>
<wire x1="-1.3918" y1="2.712" x2="-1.3918" y2="-2.688" width="0.127" layer="21"/>
<wire x1="-0.3175" y1="0.9525" x2="0.3175" y2="0.9525" width="0.127" layer="21"/>
<wire x1="0.3175" y1="0.9525" x2="0.3175" y2="-0.9525" width="0.127" layer="21"/>
<wire x1="0.3175" y1="-0.9525" x2="-0.3175" y2="-0.9525" width="0.127" layer="21"/>
<wire x1="-0.3175" y1="-0.9525" x2="-0.3175" y2="0.9525" width="0.127" layer="21"/>
<wire x1="0.9525" y1="0.9525" x2="1.5875" y2="0.9525" width="0.127" layer="21"/>
<wire x1="1.5875" y1="0.9525" x2="1.5875" y2="-0.9525" width="0.127" layer="21"/>
<wire x1="1.5875" y1="-0.9525" x2="0.9525" y2="-0.9525" width="0.127" layer="21"/>
<wire x1="0.9525" y1="-0.9525" x2="0.9525" y2="0.9525" width="0.127" layer="21"/>
<wire x1="0.127" y1="3.937" x2="0.127" y2="2.794" width="0.127" layer="51"/>
<wire x1="0.127" y1="3.937" x2="-0.127" y2="3.937" width="0.127" layer="51"/>
<wire x1="-0.127" y1="3.937" x2="-0.127" y2="2.794" width="0.127" layer="51"/>
<wire x1="1.143" y1="2.794" x2="1.143" y2="3.937" width="0.127" layer="51"/>
<wire x1="1.143" y1="3.937" x2="1.397" y2="3.937" width="0.127" layer="51"/>
<wire x1="1.397" y1="3.937" x2="1.397" y2="2.794" width="0.127" layer="51"/>
<wire x1="-0.127" y1="-2.794" x2="-0.127" y2="-3.937" width="0.127" layer="51"/>
<wire x1="-0.127" y1="-3.937" x2="0.127" y2="-3.937" width="0.127" layer="51"/>
<wire x1="0.127" y1="-3.937" x2="0.127" y2="-2.794" width="0.127" layer="51"/>
<wire x1="1.143" y1="-2.794" x2="1.143" y2="-3.937" width="0.127" layer="51"/>
<wire x1="1.143" y1="-3.937" x2="1.397" y2="-3.937" width="0.127" layer="51"/>
<wire x1="1.397" y1="-3.937" x2="1.397" y2="-2.794" width="0.127" layer="51"/>
<wire x1="4.953" y1="-2.794" x2="4.953" y2="-3.937" width="0.127" layer="51"/>
<wire x1="4.953" y1="-3.937" x2="5.207" y2="-3.937" width="0.127" layer="51"/>
<wire x1="5.207" y1="-3.937" x2="5.207" y2="-2.794" width="0.127" layer="51"/>
<wire x1="6.223" y1="-2.794" x2="6.223" y2="-3.937" width="0.127" layer="51"/>
<wire x1="6.223" y1="-3.937" x2="6.477" y2="-3.937" width="0.127" layer="51"/>
<wire x1="6.477" y1="-3.937" x2="6.477" y2="-2.794" width="0.127" layer="51"/>
<wire x1="2.413" y1="-2.794" x2="2.413" y2="-3.937" width="0.127" layer="51"/>
<wire x1="2.413" y1="-3.937" x2="2.667" y2="-3.937" width="0.127" layer="51"/>
<wire x1="2.667" y1="-3.937" x2="2.667" y2="-2.794" width="0.127" layer="51"/>
<wire x1="3.683" y1="-2.794" x2="3.683" y2="-3.937" width="0.127" layer="51"/>
<wire x1="3.683" y1="-3.937" x2="3.937" y2="-3.937" width="0.127" layer="51"/>
<wire x1="3.937" y1="-3.937" x2="3.937" y2="-2.794" width="0.127" layer="51"/>
<wire x1="2.667" y1="3.937" x2="2.667" y2="2.794" width="0.127" layer="51"/>
<wire x1="2.667" y1="3.937" x2="2.413" y2="3.937" width="0.127" layer="51"/>
<wire x1="2.413" y1="3.937" x2="2.413" y2="2.794" width="0.127" layer="51"/>
<wire x1="3.683" y1="2.794" x2="3.683" y2="3.937" width="0.127" layer="51"/>
<wire x1="3.683" y1="3.937" x2="3.937" y2="3.937" width="0.127" layer="51"/>
<wire x1="3.937" y1="3.937" x2="3.937" y2="2.794" width="0.127" layer="51"/>
<wire x1="5.207" y1="3.937" x2="5.207" y2="2.794" width="0.127" layer="51"/>
<wire x1="5.207" y1="3.937" x2="4.953" y2="3.937" width="0.127" layer="51"/>
<wire x1="4.953" y1="3.937" x2="4.953" y2="2.794" width="0.127" layer="51"/>
<wire x1="6.223" y1="2.794" x2="6.223" y2="3.937" width="0.127" layer="51"/>
<wire x1="6.223" y1="3.937" x2="6.477" y2="3.937" width="0.127" layer="51"/>
<wire x1="6.477" y1="3.937" x2="6.477" y2="2.794" width="0.127" layer="51"/>
<wire x1="2.2225" y1="0.9525" x2="2.8575" y2="0.9525" width="0.127" layer="21"/>
<wire x1="2.8575" y1="0.9525" x2="2.8575" y2="-0.9525" width="0.127" layer="21"/>
<wire x1="2.8575" y1="-0.9525" x2="2.2225" y2="-0.9525" width="0.127" layer="21"/>
<wire x1="2.2225" y1="-0.9525" x2="2.2225" y2="0.9525" width="0.127" layer="21"/>
<wire x1="3.4925" y1="0.9525" x2="4.1275" y2="0.9525" width="0.127" layer="21"/>
<wire x1="4.1275" y1="0.9525" x2="4.1275" y2="-0.9525" width="0.127" layer="21"/>
<wire x1="4.1275" y1="-0.9525" x2="3.4925" y2="-0.9525" width="0.127" layer="21"/>
<wire x1="3.4925" y1="-0.9525" x2="3.4925" y2="0.9525" width="0.127" layer="21"/>
<wire x1="4.7625" y1="0.9525" x2="5.3975" y2="0.9525" width="0.127" layer="21"/>
<wire x1="5.3975" y1="0.9525" x2="5.3975" y2="-0.9525" width="0.127" layer="21"/>
<wire x1="5.3975" y1="-0.9525" x2="4.7625" y2="-0.9525" width="0.127" layer="21"/>
<wire x1="4.7625" y1="-0.9525" x2="4.7625" y2="0.9525" width="0.127" layer="21"/>
<wire x1="6.0325" y1="0.9525" x2="6.6675" y2="0.9525" width="0.127" layer="21"/>
<wire x1="6.6675" y1="0.9525" x2="6.6675" y2="-0.9525" width="0.127" layer="21"/>
<wire x1="6.6675" y1="-0.9525" x2="6.0325" y2="-0.9525" width="0.127" layer="21"/>
<wire x1="6.0325" y1="-0.9525" x2="6.0325" y2="0.9525" width="0.127" layer="21"/>
<smd name="1" x="0" y="-3.81" dx="0.76" dy="1.27" layer="1"/>
<smd name="2" x="1.27" y="-3.81" dx="0.76" dy="1.27" layer="1"/>
<smd name="4" x="3.81" y="-3.81" dx="0.76" dy="1.27" layer="1"/>
<smd name="3" x="2.54" y="-3.81" dx="0.76" dy="1.27" layer="1"/>
<smd name="5" x="5.08" y="-3.81" dx="0.76" dy="1.27" layer="1"/>
<smd name="6" x="6.35" y="-3.81" dx="0.76" dy="1.27" layer="1"/>
<smd name="7" x="6.35" y="3.81" dx="0.76" dy="1.27" layer="1"/>
<smd name="8" x="5.08" y="3.81" dx="0.76" dy="1.27" layer="1"/>
<smd name="9" x="3.81" y="3.81" dx="0.76" dy="1.27" layer="1"/>
<smd name="10" x="2.54" y="3.81" dx="0.76" dy="1.27" layer="1"/>
<smd name="11" x="1.27" y="3.81" dx="0.76" dy="1.27" layer="1"/>
<smd name="12" x="0" y="3.81" dx="0.76" dy="1.27" layer="1"/>
<text x="-1.905" y="-2.54" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="9.525" y="-2.54" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="0.3175" y="1.5875" size="0.8128" layer="21" rot="R90">O</text>
<text x="1.5875" y="1.5875" size="0.8128" layer="21" rot="R90">N</text>
<text x="0.3175" y="-1.905" size="0.8128" layer="21" rot="R90">1</text>
<text x="1.5875" y="-1.905" size="0.8128" layer="21" rot="R90">2</text>
<text x="2.8575" y="-1.905" size="0.8128" layer="21" rot="R90">3</text>
<text x="4.1275" y="-1.905" size="0.8128" layer="21" rot="R90">4</text>
<text x="5.3975" y="-1.905" size="0.8128" layer="21" rot="R90">5</text>
<text x="6.6675" y="-1.905" size="0.8128" layer="21" rot="R90">6</text>
<rectangle x1="-0.3175" y1="-0.635" x2="0.3175" y2="0" layer="21"/>
<rectangle x1="0.9525" y1="-0.635" x2="1.5875" y2="0" layer="21"/>
<rectangle x1="2.2225" y1="-0.635" x2="2.8575" y2="0" layer="21"/>
<rectangle x1="3.4925" y1="-0.635" x2="4.1275" y2="0" layer="21"/>
<rectangle x1="4.7625" y1="-0.635" x2="5.3975" y2="0" layer="21"/>
<rectangle x1="6.0325" y1="-0.635" x2="6.6675" y2="0" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="CHS-06">
<wire x1="14.605" y1="5.08" x2="-1.905" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-1.905" y1="5.08" x2="-1.905" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-1.905" y1="-5.08" x2="14.605" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="14.605" y1="-5.08" x2="14.605" y2="5.08" width="0.4064" layer="94"/>
<wire x1="0.508" y1="-2.54" x2="-0.508" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-0.508" y1="2.54" x2="0.508" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0.508" y1="2.54" x2="0.508" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-0.508" y1="-2.54" x2="-0.508" y2="2.54" width="0.1524" layer="94"/>
<wire x1="3.048" y1="-2.54" x2="2.032" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.032" y1="2.54" x2="3.048" y2="2.54" width="0.1524" layer="94"/>
<wire x1="3.048" y1="2.54" x2="3.048" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.032" y1="-2.54" x2="2.032" y2="2.54" width="0.1524" layer="94"/>
<wire x1="5.588" y1="-2.54" x2="4.572" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="4.572" y1="2.54" x2="5.588" y2="2.54" width="0.1524" layer="94"/>
<wire x1="5.588" y1="2.54" x2="5.588" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="4.572" y1="-2.54" x2="4.572" y2="2.54" width="0.1524" layer="94"/>
<wire x1="8.128" y1="-2.54" x2="7.112" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="7.112" y1="2.54" x2="8.128" y2="2.54" width="0.1524" layer="94"/>
<wire x1="8.128" y1="2.54" x2="8.128" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="7.112" y1="-2.54" x2="7.112" y2="2.54" width="0.1524" layer="94"/>
<wire x1="10.668" y1="-2.54" x2="9.652" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="9.652" y1="2.54" x2="10.668" y2="2.54" width="0.1524" layer="94"/>
<wire x1="10.668" y1="2.54" x2="10.668" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="9.652" y1="-2.54" x2="9.652" y2="2.54" width="0.1524" layer="94"/>
<wire x1="13.208" y1="-2.54" x2="12.192" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="12.192" y1="2.54" x2="13.208" y2="2.54" width="0.1524" layer="94"/>
<wire x1="13.208" y1="2.54" x2="13.208" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="12.192" y1="-2.54" x2="12.192" y2="2.54" width="0.1524" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="17.145" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<text x="-0.254" y="-4.064" size="0.9906" layer="94" ratio="14">1</text>
<text x="2.032" y="-4.064" size="0.9906" layer="94" ratio="14">2</text>
<text x="4.572" y="-4.064" size="0.9906" layer="94" ratio="14">3</text>
<text x="7.112" y="-4.064" size="0.9906" layer="94" ratio="14">4</text>
<text x="9.779" y="-4.064" size="0.9906" layer="94" ratio="14">5</text>
<text x="-0.762" y="3.048" size="0.9906" layer="94" ratio="14">ON</text>
<text x="12.319" y="-4.064" size="0.9906" layer="94" ratio="14">6</text>
<rectangle x1="-0.254" y1="-2.286" x2="0.254" y2="0" layer="94"/>
<rectangle x1="2.286" y1="-2.286" x2="2.794" y2="0" layer="94"/>
<rectangle x1="4.826" y1="-2.286" x2="5.334" y2="0" layer="94"/>
<rectangle x1="7.366" y1="-2.286" x2="7.874" y2="0" layer="94"/>
<rectangle x1="9.906" y1="-2.286" x2="10.414" y2="0" layer="94"/>
<rectangle x1="12.446" y1="-2.286" x2="12.954" y2="0" layer="94"/>
<pin name="6" x="12.7" y="-7.62" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="7" x="12.7" y="7.62" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="8" x="10.16" y="7.62" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="9" x="7.62" y="7.62" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="10" x="5.08" y="7.62" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="1" x="0" y="-7.62" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="2" x="2.54" y="-7.62" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="3" x="5.08" y="-7.62" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="4" x="7.62" y="-7.62" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="5" x="10.16" y="-7.62" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="11" x="2.54" y="7.62" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="12" x="0" y="7.62" visible="pad" length="short" direction="pas" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CHS-06" prefix="S">
<description>Copal Electronics, Distributor Bürklin</description>
<gates>
<gate name="G$1" symbol="CHS-06" x="0" y="0"/>
</gates>
<devices>
<device name="A" package="CHS-06A">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B" package="CHS-06B">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="MJ-179PH">
<description>&lt;DC Power Connector, Jack, 4 A, 1.95 mm, PCB Mount, Through Hole&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="MJ179PH">
<description>&lt;b&gt;MJ-179PH-2&lt;/b&gt;&lt;br&gt;
</description>
<pad name="1" x="0" y="0" drill="3.16" diameter="4.74"/>
<pad name="2" x="3.05" y="4.9" drill="3.16" diameter="4.74"/>
<pad name="3" x="6" y="0" drill="3.64" diameter="5.46"/>
<text x="0.515" y="1.285" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0.515" y="1.285" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-7.7" y1="4.3" x2="6.8" y2="4.3" width="0.2" layer="51"/>
<wire x1="6.8" y1="4.3" x2="6.8" y2="-4.7" width="0.2" layer="51"/>
<wire x1="6.8" y1="-4.7" x2="-7.7" y2="-4.7" width="0.2" layer="51"/>
<wire x1="-7.7" y1="-4.7" x2="-7.7" y2="4.3" width="0.2" layer="51"/>
<wire x1="-8.7" y1="8.27" x2="9.73" y2="8.27" width="0.1" layer="51"/>
<wire x1="9.73" y1="8.27" x2="9.73" y2="-5.7" width="0.1" layer="51"/>
<wire x1="9.73" y1="-5.7" x2="-8.7" y2="-5.7" width="0.1" layer="51"/>
<wire x1="-8.7" y1="-5.7" x2="-8.7" y2="8.27" width="0.1" layer="51"/>
<wire x1="-1" y1="4.3" x2="-7.7" y2="4.3" width="0.1" layer="21"/>
<wire x1="-7.7" y1="4.3" x2="-7.7" y2="-0.5" width="0.1" layer="21"/>
<wire x1="-7.7" y1="-0.5" x2="-7.7" y2="-0.5" width="0.1" layer="21"/>
<wire x1="-7.7" y1="-0.5" x2="-7.7" y2="-4.7" width="0.1" layer="21"/>
<wire x1="-7.7" y1="-4.7" x2="6.8" y2="-4.7" width="0.1" layer="21"/>
<wire x1="6.8" y1="-4.7" x2="6.8" y2="-3.5" width="0.1" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="MJ-179PH">
<wire x1="5.08" y1="2.54" x2="15.24" y2="2.54" width="0.254" layer="94"/>
<wire x1="15.24" y1="-5.08" x2="15.24" y2="2.54" width="0.254" layer="94"/>
<wire x1="15.24" y1="-5.08" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<text x="16.51" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="16.51" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="1" x="0" y="0" length="middle"/>
<pin name="2" x="20.32" y="0" length="middle" rot="R180"/>
<pin name="3" x="0" y="-2.54" length="middle"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MJ-179PH" prefix="J">
<description>&lt;b&gt;DC Power Connector, Jack, 4 A, 1.95 mm, PCB Mount, Through Hole&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://akizukidenshi.com/download/ds/marushin/MJ179PH-MJ180PH.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="MJ-179PH" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MJ179PH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="ARROW_PART_NUMBER" value="" constant="no"/>
<attribute name="ARROW_PRICE-STOCK" value="" constant="no"/>
<attribute name="DESCRIPTION" value="DC Power Connector, Jack, 4 A, 1.95 mm, PCB Mount, Through Hole" constant="no"/>
<attribute name="HEIGHT" value="11mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Marushin Electric" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="MJ-179PH" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0.2032" drill="0">
</class>
<class number="1" name="power" width="0.254" drill="0">
</class>
</classes>
<parts>
<part name="IC1" library="WIZ850IO" deviceset="WIZ850IO" device=""/>
<part name="GND1" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="GND2" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="SUPPLY2" library="Power_Symbols" deviceset="+3.3V" device="" value="+3.3V"/>
<part name="SUPPLY4" library="Power_Symbols" deviceset="+V" device="" value="+V"/>
<part name="GND3" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="SUPPLY5" library="Power_Symbols" deviceset="+5V" device="" value="+5V"/>
<part name="U2" library="CRE1S2405SC" deviceset="CRE1S2405SC" device=""/>
<part name="SUPPLY6" library="Power_Symbols" deviceset="-V" device="" value="-V"/>
<part name="U3" library="SparkFun" deviceset="RS485" device="SOIC"/>
<part name="JP1" library="SparkFun" deviceset="M03" device="PTH"/>
<part name="GND5" library="SparkFun" deviceset="GND" device=""/>
<part name="GND9" library="SparkFun" deviceset="GND" device=""/>
<part name="C3" library="SparkFun-Capacitors" deviceset="0.1UF-25V(+80/-20%)(0603)" device="" value="0.1uF"/>
<part name="GND11" library="SparkFun" deviceset="GND" device=""/>
<part name="SUPPLY1" library="Power_Symbols" deviceset="+3.3V" device="" value="+3.3V"/>
<part name="SUPPLY3" library="Power_Symbols" deviceset="+5V" device="" value="+5V"/>
<part name="U1" library="JC_Teensy356" deviceset="TEENSY_3.5/3.6_BASIC" device=""/>
<part name="X2" library="_kantaParts" deviceset="RJ45" device="MOLEX"/>
<part name="SUPPLY11" library="Power_Symbols" deviceset="+3.3V" device="" value="+3.3V"/>
<part name="SUPPLY12" library="Power_Symbols" deviceset="+3.3V" device="" value="+3.3V"/>
<part name="R9" library="SparkFun-Resistors" deviceset="330OHM1/10W1%(0603)" device="" value="330"/>
<part name="LED3" library="SparkFun-LED" deviceset="LED-GREEN" device="0603" value="Green"/>
<part name="GND10" library="SparkFun" deviceset="GND" device=""/>
<part name="S1" library="switch-copal" deviceset="CHS-06" device="B"/>
<part name="SUPPLY7" library="Power_Symbols" deviceset="+3.3V" device="" value="+3.3V"/>
<part name="R1" library="SparkFun-Resistors" deviceset="330OHM1/10W1%(0603)" device="" value="10K"/>
<part name="R2" library="SparkFun-Resistors" deviceset="330OHM1/10W1%(0603)" device="" value="10K"/>
<part name="R3" library="SparkFun-Resistors" deviceset="330OHM1/10W1%(0603)" device="" value="10K"/>
<part name="R4" library="SparkFun-Resistors" deviceset="330OHM1/10W1%(0603)" device="" value="10K"/>
<part name="R5" library="SparkFun-Resistors" deviceset="330OHM1/10W1%(0603)" device="" value="10K"/>
<part name="R6" library="SparkFun-Resistors" deviceset="330OHM1/10W1%(0603)" device="" value="10K"/>
<part name="GND4" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="J1" library="MJ-179PH" deviceset="MJ-179PH" device=""/>
<part name="R7" library="SparkFun-Resistors" deviceset="22OHM1/10W1%(0603)" device="" value="220"/>
</parts>
<sheets>
<sheet>
<plain>
<text x="162.56" y="15.24" size="1.778" layer="91">配線:
https://www.pjrc.com/store/wiz820_sd_adaptor.html</text>
<text x="251.46" y="7.62" size="1.778" layer="91">new parts&gt;&gt;955012881</text>
</plain>
<instances>
<instance part="IC1" gate="G$1" x="175.26" y="66.04" smashed="yes">
<attribute name="NAME" x="204.47" y="73.66" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="204.47" y="71.12" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="GND1" gate="1" x="228.6" y="25.4" smashed="yes">
<attribute name="VALUE" x="228.6" y="25.146" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="GND2" gate="1" x="154.94" y="25.4" smashed="yes">
<attribute name="VALUE" x="154.94" y="25.146" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="SUPPLY2" gate="G$1" x="218.44" y="93.98" smashed="yes">
<attribute name="VALUE" x="218.313" y="97.028" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="SUPPLY4" gate="G$1" x="45.72" y="160.02" smashed="yes">
<attribute name="VALUE" x="45.72" y="160.655" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="GND3" gate="1" x="35.56" y="129.54" smashed="yes">
<attribute name="VALUE" x="35.56" y="129.286" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="SUPPLY5" gate="G$1" x="53.34" y="132.08" smashed="yes">
<attribute name="VALUE" x="53.213" y="135.128" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="U2" gate="G$1" x="66.04" y="149.86" smashed="yes">
<attribute name="NAME" x="87.63" y="157.48" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="87.63" y="154.94" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="SUPPLY6" gate="G$1" x="55.88" y="157.48" smashed="yes">
<attribute name="VALUE" x="55.88" y="160.655" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="U3" gate="G$1" x="187.96" y="147.32" smashed="yes">
<attribute name="NAME" x="180.34" y="155.448" size="1.778" layer="95"/>
<attribute name="VALUE" x="180.34" y="134.62" size="1.778" layer="96"/>
</instance>
<instance part="JP1" gate="G$1" x="241.3" y="81.28" smashed="yes">
<attribute name="VALUE" x="238.76" y="73.66" size="1.778" layer="96"/>
<attribute name="NAME" x="238.76" y="87.122" size="1.778" layer="95"/>
</instance>
<instance part="GND5" gate="1" x="248.92" y="73.66" smashed="yes">
<attribute name="VALUE" x="246.38" y="71.12" size="1.778" layer="96"/>
</instance>
<instance part="GND9" gate="1" x="205.74" y="137.16" smashed="yes">
<attribute name="VALUE" x="203.2" y="134.62" size="1.778" layer="96"/>
</instance>
<instance part="C3" gate="G$1" x="215.9" y="144.78" smashed="yes">
<attribute name="NAME" x="217.424" y="147.701" size="1.778" layer="95"/>
<attribute name="VALUE" x="217.424" y="142.621" size="1.778" layer="96"/>
</instance>
<instance part="GND11" gate="1" x="215.9" y="137.16" smashed="yes">
<attribute name="VALUE" x="213.36" y="134.62" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY1" gate="G$1" x="76.2" y="96.52" smashed="yes">
<attribute name="VALUE" x="76.073" y="99.568" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="SUPPLY3" gate="G$1" x="68.58" y="96.52" smashed="yes">
<attribute name="VALUE" x="68.453" y="99.568" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="U1" gate="G$1" x="38.1" y="66.04" smashed="yes"/>
<instance part="X2" gate="G$1" x="256.54" y="25.4" smashed="yes">
<attribute name="NAME" x="254" y="35.56" size="1.778" layer="95"/>
<attribute name="VALUE" x="254" y="12.192" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY11" gate="G$1" x="205.74" y="162.56" smashed="yes">
<attribute name="VALUE" x="205.613" y="165.608" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="SUPPLY12" gate="G$1" x="215.9" y="162.56" smashed="yes">
<attribute name="VALUE" x="215.773" y="165.608" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="R9" gate="G$1" x="157.48" y="137.16" smashed="yes" rot="MR90">
<attribute name="NAME" x="158.9786" y="133.35" size="1.778" layer="95" rot="MR90"/>
<attribute name="VALUE" x="154.178" y="133.35" size="1.778" layer="96" rot="MR90"/>
</instance>
<instance part="LED3" gate="G$1" x="157.48" y="127" smashed="yes" rot="MR0">
<attribute name="NAME" x="153.924" y="122.428" size="1.778" layer="95" rot="MR90"/>
<attribute name="VALUE" x="151.765" y="122.428" size="1.778" layer="96" rot="MR90"/>
</instance>
<instance part="GND10" gate="1" x="157.48" y="116.84" smashed="yes" rot="MR0">
<attribute name="VALUE" x="160.02" y="114.3" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="S1" gate="G$1" x="104.14" y="7.62" smashed="yes" rot="R270">
<attribute name="NAME" x="99.06" y="10.16" size="1.778" layer="95"/>
<attribute name="VALUE" x="99.06" y="-9.525" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY7" gate="G$1" x="121.92" y="15.24" smashed="yes">
<attribute name="VALUE" x="121.793" y="18.288" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="R1" gate="G$1" x="104.14" y="63.5" smashed="yes" rot="MR0">
<attribute name="NAME" x="105.41" y="62.4586" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="102.87" y="65.278" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="R2" gate="G$1" x="104.14" y="58.42" smashed="yes" rot="MR0">
<attribute name="NAME" x="105.41" y="57.3786" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="102.87" y="60.198" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="R3" gate="G$1" x="104.14" y="53.34" smashed="yes" rot="MR0">
<attribute name="NAME" x="105.41" y="52.2986" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="102.87" y="55.118" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="R4" gate="G$1" x="104.14" y="48.26" smashed="yes" rot="MR0">
<attribute name="NAME" x="105.41" y="47.2186" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="102.87" y="50.038" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="R5" gate="G$1" x="104.14" y="43.18" smashed="yes" rot="MR0">
<attribute name="NAME" x="105.41" y="42.1386" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="102.87" y="44.958" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="R6" gate="G$1" x="104.14" y="38.1" smashed="yes" rot="MR0">
<attribute name="NAME" x="105.41" y="34.5186" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="102.87" y="39.878" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="GND4" gate="1" x="119.38" y="33.02" smashed="yes">
<attribute name="VALUE" x="119.38" y="32.766" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="J1" gate="G$1" x="99.06" y="175.26" smashed="yes" rot="R180">
<attribute name="NAME" x="82.55" y="167.64" size="1.778" layer="95" rot="R180" align="center-left"/>
<attribute name="VALUE" x="82.55" y="170.18" size="1.778" layer="96" rot="R180" align="center-left"/>
</instance>
<instance part="R7" gate="G$1" x="271.78" y="73.66" smashed="yes" rot="R90">
<attribute name="NAME" x="270.2814" y="69.85" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="275.082" y="69.85" size="1.778" layer="96" rot="R90"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="GND_1"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="175.26" y1="66.04" x2="154.94" y2="66.04" width="0.1524" layer="91"/>
<wire x1="154.94" y1="66.04" x2="154.94" y2="63.5" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="GND_2"/>
<wire x1="154.94" y1="63.5" x2="154.94" y2="27.94" width="0.1524" layer="91"/>
<wire x1="175.26" y1="63.5" x2="154.94" y2="63.5" width="0.1524" layer="91"/>
<junction x="154.94" y="63.5"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="GND_3"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="208.28" y1="66.04" x2="228.6" y2="66.04" width="0.1524" layer="91"/>
<wire x1="228.6" y1="66.04" x2="228.6" y2="27.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="-VOUT"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="66.04" y1="144.78" x2="35.56" y2="144.78" width="0.1524" layer="91"/>
<wire x1="35.56" y1="144.78" x2="35.56" y2="132.08" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="248.92" y1="78.74" x2="248.92" y2="76.2" width="0.1524" layer="91"/>
<pinref part="JP1" gate="G$1" pin="1"/>
<pinref part="GND5" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="205.74" y1="142.24" x2="205.74" y2="139.7" width="0.1524" layer="91"/>
<wire x1="205.74" y1="142.24" x2="203.2" y2="142.24" width="0.1524" layer="91"/>
<pinref part="GND9" gate="1" pin="GND"/>
<pinref part="U3" gate="G$1" pin="GND"/>
</segment>
<segment>
<wire x1="215.9" y1="139.7" x2="215.9" y2="142.24" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="2"/>
<pinref part="GND11" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="17.78" y1="78.74" x2="7.62" y2="78.74" width="0.1524" layer="91"/>
<label x="7.62" y="78.74" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="GND"/>
<junction x="17.78" y="78.74"/>
</segment>
<segment>
<wire x1="76.2" y1="43.18" x2="58.42" y2="43.18" width="0.1524" layer="91"/>
<label x="66.04" y="43.18" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="GND2"/>
</segment>
<segment>
<wire x1="71.12" y1="76.2" x2="58.42" y2="76.2" width="0.1524" layer="91"/>
<label x="63.5" y="76.2" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="AGND"/>
</segment>
<segment>
<pinref part="X2" gate="G$1" pin="2"/>
<wire x1="254" y1="30.48" x2="241.3" y2="30.48" width="0.1524" layer="91"/>
<label x="241.3" y="30.48" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="157.48" y1="119.38" x2="157.48" y2="121.92" width="0.1524" layer="91"/>
<pinref part="LED3" gate="G$1" pin="C"/>
<pinref part="GND10" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="109.22" y1="63.5" x2="119.38" y2="63.5" width="0.1524" layer="91"/>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="119.38" y1="63.5" x2="119.38" y2="58.42" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="119.38" y1="58.42" x2="119.38" y2="53.34" width="0.1524" layer="91"/>
<wire x1="119.38" y1="53.34" x2="119.38" y2="48.26" width="0.1524" layer="91"/>
<wire x1="119.38" y1="48.26" x2="119.38" y2="43.18" width="0.1524" layer="91"/>
<wire x1="119.38" y1="43.18" x2="119.38" y2="38.1" width="0.1524" layer="91"/>
<wire x1="119.38" y1="38.1" x2="119.38" y2="35.56" width="0.1524" layer="91"/>
<wire x1="109.22" y1="58.42" x2="119.38" y2="58.42" width="0.1524" layer="91"/>
<junction x="119.38" y="58.42"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="109.22" y1="53.34" x2="119.38" y2="53.34" width="0.1524" layer="91"/>
<junction x="119.38" y="53.34"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="109.22" y1="48.26" x2="119.38" y2="48.26" width="0.1524" layer="91"/>
<junction x="119.38" y="48.26"/>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="109.22" y1="43.18" x2="119.38" y2="43.18" width="0.1524" layer="91"/>
<junction x="119.38" y="43.18"/>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="109.22" y1="38.1" x2="119.38" y2="38.1" width="0.1524" layer="91"/>
<junction x="119.38" y="38.1"/>
</segment>
</net>
<net name="CS" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="SCNN"/>
<wire x1="175.26" y1="55.88" x2="165.1" y2="55.88" width="0.1524" layer="91"/>
<label x="165.1" y="55.88" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="17.78" y1="50.8" x2="7.62" y2="50.8" width="0.1524" layer="91"/>
<label x="7.62" y="50.8" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="10/CS"/>
<junction x="17.78" y="50.8"/>
</segment>
</net>
<net name="MISO" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="MISO"/>
<wire x1="208.28" y1="53.34" x2="218.44" y2="53.34" width="0.1524" layer="91"/>
<label x="210.82" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="17.78" y1="45.72" x2="7.62" y2="45.72" width="0.1524" layer="91"/>
<label x="7.62" y="45.72" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="12/MISO"/>
<junction x="17.78" y="45.72"/>
</segment>
</net>
<net name="SCK" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="SCLK"/>
<wire x1="175.26" y1="58.42" x2="165.1" y2="58.42" width="0.1524" layer="91"/>
<label x="165.1" y="58.42" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="58.42" y1="45.72" x2="71.12" y2="45.72" width="0.1524" layer="91"/>
<label x="66.04" y="45.72" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="13/SCK"/>
<junction x="58.42" y="45.72"/>
</segment>
</net>
<net name="MOSI" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="MOSI"/>
<wire x1="175.26" y1="60.96" x2="165.1" y2="60.96" width="0.1524" layer="91"/>
<label x="165.1" y="60.96" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="7.62" y1="48.26" x2="17.78" y2="48.26" width="0.1524" layer="91"/>
<label x="7.62" y="48.26" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="11/MOSI"/>
<junction x="17.78" y="48.26"/>
</segment>
</net>
<net name="+3.3V" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="3.3V_1"/>
<pinref part="SUPPLY2" gate="G$1" pin="+3.3V"/>
<wire x1="208.28" y1="63.5" x2="218.44" y2="63.5" width="0.1524" layer="91"/>
<wire x1="218.44" y1="63.5" x2="218.44" y2="93.98" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="3.3V_2"/>
<wire x1="208.28" y1="60.96" x2="218.44" y2="60.96" width="0.1524" layer="91"/>
<wire x1="218.44" y1="60.96" x2="218.44" y2="63.5" width="0.1524" layer="91"/>
<junction x="218.44" y="63.5"/>
</segment>
<segment>
<pinref part="SUPPLY1" gate="G$1" pin="+3.3V"/>
<wire x1="58.42" y1="73.66" x2="76.2" y2="73.66" width="0.1524" layer="91"/>
<wire x1="76.2" y1="73.66" x2="76.2" y2="96.52" width="0.1524" layer="91"/>
<label x="60.96" y="73.66" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="3V3_LO"/>
<junction x="58.42" y="73.66"/>
</segment>
<segment>
<wire x1="203.2" y1="149.86" x2="205.74" y2="149.86" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="VCC"/>
<pinref part="SUPPLY11" gate="G$1" pin="+3.3V"/>
<wire x1="205.74" y1="162.56" x2="205.74" y2="149.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C3" gate="G$1" pin="1"/>
<pinref part="SUPPLY12" gate="G$1" pin="+3.3V"/>
<wire x1="215.9" y1="162.56" x2="215.9" y2="149.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY7" gate="G$1" pin="+3.3V"/>
<pinref part="S1" gate="G$1" pin="12"/>
<wire x1="121.92" y1="15.24" x2="121.92" y2="7.62" width="0.1524" layer="91"/>
<wire x1="121.92" y1="7.62" x2="111.76" y2="7.62" width="0.1524" layer="91"/>
<pinref part="S1" gate="G$1" pin="11"/>
<wire x1="121.92" y1="7.62" x2="121.92" y2="5.08" width="0.1524" layer="91"/>
<wire x1="121.92" y1="5.08" x2="111.76" y2="5.08" width="0.1524" layer="91"/>
<junction x="121.92" y="7.62"/>
<pinref part="S1" gate="G$1" pin="10"/>
<wire x1="121.92" y1="5.08" x2="121.92" y2="2.54" width="0.1524" layer="91"/>
<wire x1="121.92" y1="2.54" x2="111.76" y2="2.54" width="0.1524" layer="91"/>
<junction x="121.92" y="5.08"/>
<pinref part="S1" gate="G$1" pin="9"/>
<wire x1="121.92" y1="2.54" x2="121.92" y2="0" width="0.1524" layer="91"/>
<wire x1="121.92" y1="0" x2="111.76" y2="0" width="0.1524" layer="91"/>
<junction x="121.92" y="2.54"/>
<pinref part="S1" gate="G$1" pin="8"/>
<wire x1="121.92" y1="0" x2="121.92" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="121.92" y1="-2.54" x2="111.76" y2="-2.54" width="0.1524" layer="91"/>
<junction x="121.92" y="0"/>
<pinref part="S1" gate="G$1" pin="7"/>
<wire x1="121.92" y1="-2.54" x2="121.92" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="121.92" y1="-5.08" x2="111.76" y2="-5.08" width="0.1524" layer="91"/>
<junction x="121.92" y="-2.54"/>
</segment>
</net>
<net name="RST_WIZ" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="RSTN"/>
<wire x1="208.28" y1="55.88" x2="223.52" y2="55.88" width="0.1524" layer="91"/>
<label x="210.82" y="55.88" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="17.78" y1="53.34" x2="7.62" y2="53.34" width="0.1524" layer="91"/>
<label x="7.62" y="53.34" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="9/CS"/>
<junction x="17.78" y="53.34"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="+VOUT"/>
<wire x1="66.04" y1="142.24" x2="63.5" y2="142.24" width="0.1524" layer="91"/>
<wire x1="63.5" y1="142.24" x2="63.5" y2="127" width="0.1524" layer="91"/>
<pinref part="SUPPLY5" gate="G$1" pin="+5V"/>
<wire x1="63.5" y1="127" x2="53.34" y2="127" width="0.1524" layer="91"/>
<wire x1="53.34" y1="127" x2="53.34" y2="132.08" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="58.42" y1="78.74" x2="68.58" y2="78.74" width="0.1524" layer="91"/>
<wire x1="68.58" y1="78.74" x2="68.58" y2="96.52" width="0.1524" layer="91"/>
<pinref part="SUPPLY3" gate="G$1" pin="+5V"/>
<pinref part="U1" gate="G$1" pin="VIN"/>
<junction x="58.42" y="78.74"/>
</segment>
</net>
<net name="-V" class="0">
<segment>
<pinref part="SUPPLY6" gate="G$1" pin="-V"/>
<wire x1="55.88" y1="157.48" x2="55.88" y2="149.86" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="-VIN"/>
<wire x1="55.88" y1="149.86" x2="66.04" y2="149.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="1"/>
<wire x1="99.06" y1="175.26" x2="111.76" y2="175.26" width="0.1524" layer="91"/>
<label x="109.22" y="175.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="+V" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="+VIN"/>
<wire x1="66.04" y1="147.32" x2="45.72" y2="147.32" width="0.1524" layer="91"/>
<pinref part="SUPPLY4" gate="G$1" pin="+V"/>
<wire x1="45.72" y1="147.32" x2="45.72" y2="157.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="3"/>
<wire x1="99.06" y1="177.8" x2="111.76" y2="177.8" width="0.1524" layer="91"/>
<label x="106.68" y="177.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="RTS" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="2"/>
<wire x1="17.78" y1="71.12" x2="7.62" y2="71.12" width="0.1524" layer="91"/>
<label x="7.62" y="71.12" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R9" gate="G$1" pin="2"/>
<label x="152.4" y="142.24" size="1.778" layer="95"/>
<wire x1="157.48" y1="142.24" x2="157.48" y2="147.32" width="0.1524" layer="91"/>
<wire x1="177.8" y1="144.78" x2="175.26" y2="144.78" width="0.1524" layer="91"/>
<wire x1="175.26" y1="144.78" x2="175.26" y2="147.32" width="0.1524" layer="91"/>
<wire x1="177.8" y1="147.32" x2="175.26" y2="147.32" width="0.1524" layer="91"/>
<wire x1="175.26" y1="147.32" x2="165.1" y2="147.32" width="0.1524" layer="91"/>
<junction x="175.26" y="147.32"/>
<pinref part="U3" gate="G$1" pin="DE"/>
<pinref part="U3" gate="G$1" pin="/RE"/>
<label x="165.1" y="147.32" size="1.778" layer="95"/>
<wire x1="157.48" y1="147.32" x2="175.26" y2="147.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="A" class="0">
<segment>
<wire x1="203.2" y1="144.78" x2="208.28" y2="144.78" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="A"/>
<label x="205.74" y="144.78" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP1" gate="G$1" pin="2"/>
<wire x1="248.92" y1="81.28" x2="259.08" y2="81.28" width="0.1524" layer="91"/>
<wire x1="259.08" y1="81.28" x2="259.08" y2="66.04" width="0.1524" layer="91"/>
<wire x1="259.08" y1="66.04" x2="271.78" y2="66.04" width="0.1524" layer="91"/>
<wire x1="271.78" y1="66.04" x2="271.78" y2="68.58" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="271.78" y1="66.04" x2="279.4" y2="66.04" width="0.1524" layer="91"/>
<label x="276.86" y="66.04" size="1.778" layer="95"/>
<junction x="271.78" y="66.04"/>
<label x="251.46" y="81.28" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="X2" gate="G$1" pin="3"/>
<wire x1="254" y1="27.94" x2="241.3" y2="27.94" width="0.1524" layer="91"/>
<label x="241.3" y="27.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="B" class="0">
<segment>
<wire x1="203.2" y1="147.32" x2="208.28" y2="147.32" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="B"/>
<label x="205.74" y="147.32" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP1" gate="G$1" pin="3"/>
<wire x1="271.78" y1="83.82" x2="271.78" y2="78.74" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="271.78" y1="83.82" x2="279.4" y2="83.82" width="0.1524" layer="91"/>
<label x="276.86" y="83.82" size="1.778" layer="95"/>
<junction x="271.78" y="83.82"/>
<wire x1="248.92" y1="83.82" x2="271.78" y2="83.82" width="0.1524" layer="91"/>
<label x="251.46" y="83.82" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="X2" gate="G$1" pin="6"/>
<wire x1="254" y1="20.32" x2="241.3" y2="20.32" width="0.1524" layer="91"/>
<label x="241.3" y="20.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="RX-I" class="0">
<segment>
<wire x1="177.8" y1="142.24" x2="165.1" y2="142.24" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="DI"/>
<label x="165.1" y="142.24" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="1/TX"/>
<wire x1="17.78" y1="73.66" x2="7.62" y2="73.66" width="0.1524" layer="91"/>
<label x="7.62" y="73.66" size="1.778" layer="95"/>
</segment>
</net>
<net name="TX-O" class="0">
<segment>
<wire x1="177.8" y1="149.86" x2="165.1" y2="149.86" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="RO"/>
<label x="165.1" y="149.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="0/RX"/>
<wire x1="17.78" y1="76.2" x2="7.62" y2="76.2" width="0.1524" layer="91"/>
<label x="7.62" y="76.2" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<wire x1="157.48" y1="129.54" x2="157.48" y2="132.08" width="0.1524" layer="91"/>
<pinref part="R9" gate="G$1" pin="1"/>
<pinref part="LED3" gate="G$1" pin="A"/>
</segment>
</net>
<net name="IO_0" class="0">
<segment>
<pinref part="S1" gate="G$1" pin="1"/>
<wire x1="96.52" y1="7.62" x2="86.36" y2="7.62" width="0.1524" layer="91"/>
<label x="86.36" y="7.62" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="58.42" y1="50.8" x2="86.36" y2="50.8" width="0.1524" layer="91"/>
<wire x1="86.36" y1="50.8" x2="86.36" y2="38.1" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="86.36" y1="38.1" x2="99.06" y2="38.1" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="15/A1"/>
<label x="66.04" y="50.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="IO_1" class="0">
<segment>
<pinref part="S1" gate="G$1" pin="2"/>
<wire x1="96.52" y1="5.08" x2="86.36" y2="5.08" width="0.1524" layer="91"/>
<label x="86.36" y="5.08" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="58.42" y1="53.34" x2="88.9" y2="53.34" width="0.1524" layer="91"/>
<wire x1="88.9" y1="53.34" x2="88.9" y2="43.18" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="88.9" y1="43.18" x2="99.06" y2="43.18" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="16/A2"/>
<label x="66.04" y="53.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="IO_2" class="0">
<segment>
<pinref part="S1" gate="G$1" pin="3"/>
<wire x1="96.52" y1="2.54" x2="86.36" y2="2.54" width="0.1524" layer="91"/>
<label x="86.36" y="2.54" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="58.42" y1="55.88" x2="91.44" y2="55.88" width="0.1524" layer="91"/>
<wire x1="91.44" y1="55.88" x2="91.44" y2="48.26" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="91.44" y1="48.26" x2="99.06" y2="48.26" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="17/A3"/>
<label x="66.04" y="55.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="IO_3" class="0">
<segment>
<pinref part="S1" gate="G$1" pin="4"/>
<wire x1="96.52" y1="0" x2="86.36" y2="0" width="0.1524" layer="91"/>
<label x="86.36" y="0" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="58.42" y1="58.42" x2="93.98" y2="58.42" width="0.1524" layer="91"/>
<wire x1="93.98" y1="58.42" x2="93.98" y2="53.34" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="93.98" y1="53.34" x2="99.06" y2="53.34" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="18/A4/SDA"/>
<label x="66.04" y="58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="IO_4" class="0">
<segment>
<pinref part="S1" gate="G$1" pin="5"/>
<wire x1="96.52" y1="-2.54" x2="86.36" y2="-2.54" width="0.1524" layer="91"/>
<label x="86.36" y="-2.54" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="58.42" y1="60.96" x2="96.52" y2="60.96" width="0.1524" layer="91"/>
<wire x1="96.52" y1="60.96" x2="96.52" y2="58.42" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="96.52" y1="58.42" x2="99.06" y2="58.42" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="19/A5/SCL"/>
<label x="66.04" y="60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="IO_5" class="0">
<segment>
<pinref part="S1" gate="G$1" pin="6"/>
<wire x1="96.52" y1="-5.08" x2="86.36" y2="-5.08" width="0.1524" layer="91"/>
<label x="86.36" y="-5.08" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="58.42" y1="63.5" x2="99.06" y2="63.5" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="2"/>
<pinref part="U1" gate="G$1" pin="20/A6"/>
<label x="66.04" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="113,1,245.703,82.8396,JP1,,,,,"/>
</errors>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
